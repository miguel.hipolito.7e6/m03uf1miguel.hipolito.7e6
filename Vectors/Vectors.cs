﻿/*
 * AUTHOR: Miguel Á. Hipólito
 * DATE: 2022/11/08
 * DESCRIPTION: Menu per navegar entre métodes que ultitzen vectors
 */


using System;
using System.Linq;

namespace Vectors
{
    class Vectors
    {
        //Donat un enter, printa el dia de la setmana amb text (dilluns, dimarts, dimecres…), tenint en compte que dilluns és el 0. Els dies de la setmana es guarden en un vector.
        public void DayOfWeek()
        {
            string[] dies = { "dilluns", "dimarts", "dimecres", "dijous", "divendres", "dissabte", "diumenge" };

            int nombre;
            do
            {
                Console.WriteLine("Inserta nombre entre 0 i 6");
                nombre = Convert.ToInt32(Console.ReadLine());

            } while (nombre < 0 || nombre > 6);
            
            Console.WriteLine(dies[nombre]);
        }

        //Volem fer un petit programa per guardar l'alineació inicial de jugadors d'un partit de bàsquet.L'usuari introduirà els 5 números dels jugadors. Imprimeix-los després amb el format indicat.
        public void PlayerNumbers()
        {
            int[] nombre = new int [5];

            for (int index = 0; index < nombre.Length; index++)
            {
                Console.WriteLine("Inserta nombre del jugador");
                nombre[index] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("La alineació es:");
            Console.Write("[");
            int x = 0;
            for (int index = 0; index < nombre.Length; index++)
            {
                Console.Write(nombre[index]);
                
                if (x < 4)
                {
                    Console.Write(", ");
                    x++;
                }
            }
            Console.Write("]");               
        }

        //Volem fer un petit programa per un partit polític. Quan hi ha eleccions, el partit presenta una llista de candidats. Cada candidat té una posició a la llista.
        public void CandidatesList()
        {
            int ncandidats;
            do
            {
                Console.WriteLine("Inserta nombre de candidats:");
                ncandidats = Convert.ToInt32(Console.ReadLine());

            } while (ncandidats <= 0);
            
            string[] nombre = new string[ncandidats];

            for (int index = 0; index < ncandidats; index++)
            {
                Console.WriteLine("Inserta el nom:");
                nombre[index] = Console.ReadLine();
            }

            int i;
            do 
            {
                Console.WriteLine("Quines posicions vols?: ");
                i = Convert.ToInt32(Console.ReadLine());
                
                if (i > 0 && i <= ncandidats) Console.WriteLine(nombre[i - 1]);
                Console.WriteLine("-------");
            } while (i != -1);
        }

        //Donada una paraula i una posició  indica quina lletra hi ha a la posició indicada
        public void LetterInWord()
        {
            Console.WriteLine("Inserta una paraula:");
            string paraula = Convert.ToString(Console.ReadLine());

            int posicio;
            do
            {
                Console.WriteLine("Inserta la posició de lletra que vols:");
                posicio = Convert.ToInt32(Console.ReadLine());
            } while (posicio <= 0 || posicio > paraula.Length);

            Console.WriteLine(paraula[posicio -1]); //* el string puede actuar como vector
        }

        //Inicialitza un vector de floats de mida 50, amb el valor 0.0f a tots els elements.
        public void AddValuesToList()
        {
            double[] vector = new double[50];

            vector[0] = 31.0;
            vector[1] = 56.0;
            vector[19] = 12.0;
            vector[vector.Length - 1] = 79.0;

            Console.Write("[");
            int x = 0;
            for (int index = 0; index < vector.Length; index++)
            {
                Console.Write(vector[index]);

                if (x < 49)
                {
                    Console.Write(", ");
                    x++;
                }
            }
            Console.Write("]");
        }

        //Donada un vector de 4 números de tipus int,intercanvia el primer per l'últim element.
        public void Swap()
        {
            int[] vector = new int[4];

            for (int index = 0; index < vector.Length; index++)
            {
                Console.WriteLine("Inserta diversos nombres");
                vector[index] = Convert.ToInt32(Console.ReadLine());
            }

            int a = vector[vector.Length - 1];

            vector[vector.Length - 1] = vector[0];
            vector[0] = a;

            Console.Write("[");
            int x = 0;
            for (int index = 0; index < vector.Length; index++)
            {
                Console.Write(vector[index]);

                if (x < vector.Length - 1)
                {
                    Console.Write(", ");
                    x++;
                }
            }
            Console.Write("]");
        }

        //Volem fer un simulador d'un candau com el de la foto: La nostra versió, també tindrà 8 botons, però el primer serà el 0. A l'inici tots els botons estaran sense prémer.
        public void PushButtonPadlockSimulator()
        {
            bool[] botons = new bool [8];

            int pres;
            do
            {
                Console.WriteLine("Inserta qualsevol botó 0-7");
                pres = Convert.ToInt32(Console.ReadLine());
                if (pres >= 0 && pres <= 7)
                {
                    botons[pres] = !botons[pres];
                }
            } while (pres != -1);

            Console.Write("[");
            int x = 0;
            for (int index = 0; index < botons.Length; index++)
            {
                Console.Write(botons[index]);

                if (x < botons.Length - 1)
                {
                    Console.Write(", ");
                    x++;
                }
            }
            Console.Write("]");
        }

        //Un banc té tot de caixes de seguretat, enumerades del 0 al 10. Volem registrar quan els usuaris obren una caixa de seguretat, i al final del dia, fer-ne un recompte.

        public void BoxesOpenedCounter()
        {
            int[] botons = new int[10];

            int pres;
            do
            {
                Console.WriteLine("Inserta qualsevol caixa 0-10:");
                pres = Convert.ToInt32(Console.ReadLine());
                if (pres >= 0 && pres <= 10)
                {
                    botons[pres] += 1;
                }
            } while (pres != -1);

            Console.Write("[");
            int x = 0;
            for (int index = 0; index < botons.Length; index++)
            {
                Console.Write(botons[index]);

                if (x < botons.Length - 1)
                {
                    Console.Write(", ");
                    x++;
                }
            }
            Console.Write("]");
        }

        //L'usuari entra 10 enters. Crea un vector amb aquest valors. Imprimeix per pantalla el valor més petit introduït.
        public void MinOf10Values()
        {
            int[] vector = new int[10];

            for (int index = 0; index < vector.Length; index++)
            {
                Console.WriteLine("Introdueix 10 nombres:");
                vector[index] = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("-----");
            Console.WriteLine(vector.Min()); //* Usar system.linq para el metodo
        }

        //Donat el següent vector, imprimeix true si algun dels números és divisible entre 7 o false en cas contrari.
        public void IsThereAMultipleOf7()
        {
            int[] values = { 4, 8, 9, 40, 54, 84, 40, 6, 84, 1, 1, 68, 84, 68, 4, 840, 684, 25, 40, 98, 54, 687, 31, 4894, 468, 46, 84687, 894, 40, 846, 1681, 618, 161, 846, 84687, 6, 848 };
            bool resultat = false;
            for (int index = 0; index < values.Length; index++)
            {
                if (values[index] % 7 == 0) resultat = true; 
            }
            Console.WriteLine("------");
            Console.WriteLine(resultat);
        }

        //Donat una llista d'enters ordenats de menor a major indica si un cert valor existeix en el bucle.
        public void SearchInOrdered()
        {
            bool resultat = false;

            Console.WriteLine("Introdueix nº de valors del array:");
            int nvalors = Convert.ToInt32(Console.ReadLine());

            int[] vector = new int[nvalors];

            for (int index = 0; index < vector.Length; index++)
            {
                Console.WriteLine("Introdueix els seus valors:");
                vector[index] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Introdueix el nombre que vols comprobar:");
            int comprobar = Convert.ToInt32(Console.ReadLine());

            for (int index = 0; index < vector.Length; index++)
            {
                if (vector[index] == comprobar) resultat = true;
            }
            Console.WriteLine("------");
            Console.WriteLine(resultat);
        }

        //L'usuari entra 10 enters. Imprimeix-los en l'ordre invers al que els ha entrat.
        public void InverseOrder()
        {
            int[] vector = new int[10];

            for (int index = 0; index < vector.Length; index++)
            {
                Console.WriteLine("Introdueix 10 nombres:");
                vector[index] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("---------");

            for (int index = vector.Length - 1; index >= 0; index--)
            {
                Console.Write(vector[index]+ " ");
            }
        }

        //Indica si una paraula és palíndrom.
        public void Palindrome()
        {
            Console.WriteLine("Introdueix una paraula:");
            string paraula = Console.ReadLine();
            paraula = paraula.ToLower();

            int sumador = 0;
            int i = 0;
            int j = paraula.Length - 1;

            for (;i < paraula.Length && j >= 0 ;i++, j--)
            {
                if (paraula[i] == paraula[j]) sumador++; 
            }

            Console.WriteLine("------");
            Console.WriteLine(sumador == paraula.Length);
        }

        //Printa per pantalla ordenats si la llista de N valors introduïts per l'usuari estan ordenats.
        public void ListSortedValues()
        {
            Console.WriteLine("Introdueix nº de valors del array:");
            int nvalors = Convert.ToInt32(Console.ReadLine());

            int[] vector = new int[nvalors];

            for (int index = 0; index < vector.Length; index++)
            {
                Console.WriteLine("Introdueix els seus valors:");
                vector[index] = Convert.ToInt32(Console.ReadLine());
            }

            int sumador = 0;

            for (int index = 1; index < vector.Length; index++)
            {
                if (vector[index] >= vector[index - 1]) sumador++;
            }

            Console.WriteLine("------");
            if (sumador == nvalors - 1)
            {
                Console.WriteLine("ordenats");
            }
            else Console.WriteLine("desordenats");
        }

        //Printa per pantalla cap i cua si la llista de N valors introduïts per l'usuari són cap i cua (llegits en ordre invers és la mateixa llista).
        public void CapICuaValues()
        {
            Console.WriteLine("Introdueix nº de valors del array:");
            int nvalors = Convert.ToInt32(Console.ReadLine());

            int[] vector = new int[nvalors];

            for (int index = 0; index < vector.Length; index++)
            {
                Console.WriteLine("Introdueix els seus valors:");
                vector[index] = Convert.ToInt32(Console.ReadLine());
            }

            int sumador = 0;
            int i = 0;
            int j = vector.Length - 1;

            for (; i < vector.Length && j >= 0; i++, j--)
            {
                if (vector[i] == vector[j]) sumador++;
            }

            Console.WriteLine("------");
            if (sumador == vector.Length)
            {
                Console.WriteLine("cap i cua");
            }
            else Console.WriteLine("NO cap i cua :[");
        }

        //L'usuari introduirà 2 vectors de valors, primer mida després introdueix elements. Printa per pantalla són iguals si ha introduït el mateix vector, o no són iguals si són diferents.
        public void ListSameValues()
        {
            Console.WriteLine("Introdueix nº de valors del vector [1]:");
            int nvalors1 = Convert.ToInt32(Console.ReadLine());

            int[] vector1 = new int[nvalors1];

            for (int index = 0; index < vector1.Length; index++)
            {
                Console.WriteLine("Introdueix els seus valors:");
                vector1[index] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Introdueix nº de valors del vector [2]:");
            int nvalors2 = Convert.ToInt32(Console.ReadLine());

            int[] vector2 = new int[nvalors2];

            for (int index = 0; index < vector2.Length; index++)
            {
                Console.WriteLine("Introdueix els seus valors:");
                vector2[index] = Convert.ToInt32(Console.ReadLine());
            }

            int sumador = 0;
            for (int i = 0; i < vector1.Length; i++)
            {
                if (vector1[i] == vector2[i]) sumador++;
            }

            Console.WriteLine("---------");
            if (sumador == vector1.Length)
            {
                Console.WriteLine("son iguals");
            }
            else Console.WriteLine("NO son iguals :[");
        }

        //L'usuari introdueix una llista de valors. Imprimeix per pantalla la suma d'aquests valors.

        public void ListSumValues()
        {
            Console.WriteLine("Introdueix nº de valors del array:");
            int nvalors = Convert.ToInt32(Console.ReadLine());

            int[] vector = new int[nvalors];
            int resultat = 0;

            for (int index = 0; index < vector.Length; index++)
            {
                Console.WriteLine("Introdueix 4 nombres:");
                vector[index] = Convert.ToInt32(Console.ReadLine());

                resultat = resultat + vector[index];
            }

            Console.WriteLine("---------");
            Console.WriteLine(resultat);
        }

        //En una botiga volem convertir tot de preus sense a IVA al preu amb IVA. Per afegir l'IVA a un preu hem de sumar-hi el 21% del seu valor.
        public void IvaPrices()
        {
            double[] preus = new double[10];

            for (int index = 0; index < preus.Length; index++)
            {
                Console.WriteLine("Introdueix 10 nombres:");
                preus[index] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("---------");

            for (int index = 0; index < preus.Length; index++)
            {
                double resultat = preus[index] * 1.21;
                Console.WriteLine(preus[index] + " * IVA = " + Math.Round(resultat, 2).ToString("#.00"));
            }
        }

        //El departament de salut ens ha demanat que calculem la taxa d’infecció que està tenint la Covid en la nostre regió sanitària. Donat un nombre de casos 
        public void CovidGrowRate()
        {
            Console.WriteLine("Introdueix nº de valors del array:");
            int nvalors = Convert.ToInt32(Console.ReadLine());

            double[] casos = new double[nvalors];

            for (int index = 0; index < casos.Length; index++)
            {
                Console.WriteLine("Introdueix els seus valors:");
                casos[index] = Convert.ToInt32(Console.ReadLine());
            }

            for (int index = 1; index < casos.Length; index++)
            {
                Console.WriteLine(casos[index] / casos[index - 1]);
            }
        }

        //Donada una velocitat d'una bicicleta en metres per segon, indica els metres que haurà recorregut quan hagi passat 1,2,3,4,5,6,7,8,9 i 10 segons.
        public void BicicleDistance()
        {
            Console.WriteLine("Intrueix la velocitat (mru):");
            double velocitat = Convert.ToDouble(Console.ReadLine());

            double resultat = 0;
            for (int contador = 1; contador <= 10; contador++)
            {
                resultat = resultat + velocitat;
                Console.Write(resultat + " ");
            }
        }

        //L'usuari introdueix una llista de valors. Imprimeix per pantalla el valor que està més proper a la mitjana dels valors de la llista (calcula la mitjana dels valors primer i cerca el més proper després).
        public void ValueNearAvg()
        {
            Console.WriteLine("Introdueix nº de valors del array:");
            int nvalors = Convert.ToInt32(Console.ReadLine());

            double[] valors = new double[nvalors];

            double resultat = 0;
            for (int index = 0; index < valors.Length; index++)
            {
                Console.WriteLine("Introdueix els seus valors:");
                valors[index] = Convert.ToInt32(Console.ReadLine());

                resultat = resultat + valors[index];
            }

            resultat = resultat / nvalors;

            double[] diferencia = new double[nvalors];

            double comparador = valors.Sum();
            int final = 0;

            for (int index = 0; index < diferencia.Length; index++)
            {
                diferencia[index] = Math.Abs(resultat - valors[index]);
                if (diferencia[index] < comparador)
                {
                    comparador = diferencia[index];
                    final = index;                    
                }
            }


            Console.WriteLine("---------");
            Console.WriteLine(valors[final]);

        }

        //Es vol fer un programa per verificar que un codi ISBN (International Standard Book Code) és correcte. El format d’un codi ISBN és: Codi de grup (1 dígit), Codi de l’editor (4 dígits), Codi del llibre (4 dígits), Caràcter/dígit de control (1 caràcter/dígit)
        public void Isbn()
        {
            int[] vector = new int[9];
            int resultat = 0;

            for (int index = 0; index < vector.Length; index++)
            {
                Console.WriteLine("Introdueix 9 nombres:");
                vector[index] = Convert.ToInt32(Console.ReadLine());
                resultat = resultat + vector[index] * (index + 1);
            }

            resultat = resultat % 11;

            Console.WriteLine("Introdueix el de seguretat");
            char seguretat = Convert.ToChar(Console.ReadLine());

            int intseguretat = seguretat - '0';

            if (resultat == intseguretat)
            {
                Console.WriteLine("vàlid");
            }
            else
            {
                if (resultat == 10 && seguretat == 'x')
                {
                    Console.WriteLine("vàlid");
                }
                else Console.WriteLine("NO vàlid");
            }
        }

        //A partir de l'any 2006 els ISBN van passar a tenir 13 dígits i es va adaptar per a què fos compatible amb el sistema de codi de barres EAN-13. Volem verificar si un codi ISBN és correcte.Mitjançant el dígit de control que és el dígit número 13. Es calcula de la manera següent:
        public void Isbn13()
        {
            int[] vector = new int[13];
            int resultat = 0;

            for (int index = 0, a = 1; index < vector.Length - 1; index++)
            {
                Console.WriteLine("Introdueix 12 nombres:");
                vector[index] = Convert.ToInt32(Console.ReadLine());
                resultat = resultat + vector[index] * (a);
                if (a == 1) a += 2; else a -= 2;
            }

            Console.WriteLine("Introdueix el de seguretat");
            vector[12] = Convert.ToInt32(Console.ReadLine());

            if ((resultat + vector[12]) % 10 == 0)
            {
                Console.WriteLine("vàlid");
            }
            else Console.WriteLine("NO valid");   
        }

        public void Menu()
        {
            string opcio;
            do
            {
                Console.WriteLine("0.- FINALITZAR");
                Console.WriteLine("1.- DayOfWeek");
                Console.WriteLine("2.- PlayerNumbers");
                Console.WriteLine("3.- CandidatesList");
                Console.WriteLine("4.- LetterInWord");
                Console.WriteLine("5.- AddValuesToList");
                Console.WriteLine("6.- Swap");
                Console.WriteLine("7.- PushButtonPadlockSimulator");
                Console.WriteLine("8.- BoxesOpenedCounter");
                Console.WriteLine("9.- MinOf10Values");
                Console.WriteLine("A.- IsThereAMultipleOf7");
                Console.WriteLine("B.- SearchInOrdered");
                Console.WriteLine("C.- InverseOrder");
                Console.WriteLine("D.- Palindrome");
                Console.WriteLine("E.- ListSortedValues");
                Console.WriteLine("F.- CapICuaValues");
                Console.WriteLine("G.- ListSameValues");
                Console.WriteLine("H.- ListSumValues");
                Console.WriteLine("I.- IvaPrices");
                Console.WriteLine("J.- CovidGrowRate");
                Console.WriteLine("K.- BicicleDistance");
                Console.WriteLine("L.- ValueNearAvg");
                Console.WriteLine("M.- Isbn");
                Console.WriteLine("N.- Isbn13");
                 
                opcio = Console.ReadLine();
                opcio = opcio.ToUpper();
                Console.Clear();
                switch (opcio)
                {
                    case "0": Console.WriteLine("bye");
                        break;
                    case "1":
                        DayOfWeek();
                        break;
                    case "2":
                        PlayerNumbers();
                        break;
                    case "3":
                        CandidatesList();
                        break;
                    case "4":
                        LetterInWord();
                        break;
                    case "5":
                        AddValuesToList();
                        break;
                    case "6":
                        Swap();
                        break;
                    case "7":
                        PushButtonPadlockSimulator();
                        break;
                    case "8":
                        BoxesOpenedCounter();
                        break;
                    case "9":
                        MinOf10Values();
                        break;
                    case "A":
                        IsThereAMultipleOf7();
                        break;
                    case "B":
                        SearchInOrdered();
                        break;
                    case "C":
                        InverseOrder();
                        break;
                    case "D":
                        Palindrome();
                        break;
                    case "E":
                        ListSortedValues();
                        break;
                    case "F":
                        CapICuaValues();
                        break;
                    case "G":
                        ListSameValues();
                        break;
                    case "H":
                        ListSumValues();
                        break;
                    case "I":
                        IvaPrices();
                        break;
                    case "J":
                        CovidGrowRate();
                        break;
                    case "K":
                        BicicleDistance();
                        break;
                    case "L":
                        ValueNearAvg();
                        break;
                    case "M":
                        Isbn();
                        break;
                    case "N":
                        Isbn13();
                        break;
                    default: Console.WriteLine("Opció Incorrecta");
                        break;
                }
                Console.ReadLine();
                Console.Clear();

            } while (opcio != "0");
        }

        public static void Main() 
        {
            var menu = new Vectors();  /*per poder utiltzar l'objecte menu, necessitem un objecte que el cridi*/
            menu.Menu();               /*ir a Menu*/
        }

        /* es pot utilitzar directament amb statics tot.
        /* public static void Main()
         {
            Vectors.Menu();    directament classe.metode (especificant que es static el metode)        
         }
        */
    }
}