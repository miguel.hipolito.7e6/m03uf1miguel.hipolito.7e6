﻿/*
 * AUTHOR: Miguel Á. Hipólito
 * DATE: 2022/11/30
 * DESCRIPTION: Menu per navegar entre métodes que ultitzen vectors
 */

using System;
using System.Linq;

namespace Cadenes
{
    class Cadenes
    {
        //Feu un programa que rebi per l’entrada 2 seqüències de caràcters i digui si aquestes són iguals.
        public void SonIguals()
        {
            Console.WriteLine("Escriu dues paraula:");

            Console.Write("La primera: ");
            string primeraParaula = Console.ReadLine();
            Console.Write("La segona:  ");
            string segonaParaula = Console.ReadLine();

            
            if (primeraParaula == segonaParaula)
            {
                Console.WriteLine("Són iguals");
            }
            else Console.WriteLine("No són iguals");
        }

        //Feu un programa que rebi per l’entrada 2 seqüències de caràcters i digui si aquestes són iguals, sense tenir en compte majúscules i minúscules.
        public void SonIguals2()
        {
            Console.WriteLine("Escriu dues paraula:");

            Console.Write("La primera: ");
            string primeraParaula = Console.ReadLine();
            primeraParaula = primeraParaula.ToLower();
            Console.Write("La segona:  ");
            string segonaParaula = Console.ReadLine();
            segonaParaula = segonaParaula.ToLower();


            if (primeraParaula == segonaParaula)
            {
                Console.WriteLine("Són iguals");
            }
            else Console.WriteLine("No són iguals");
        }

        //Feu un programa que rebi per l’entrada 1 String i després una serie indefinida de caràcters per anar traient de l’String, si és que el conté, fins que es rep un 0.
        public void PurgaDeCaracters()
        {
            Console.Write("Escriu una paraula: ");
            string paraula = Console.ReadLine();

            string lletra;
            do
            {
                Console.Write("Lletra eliminada (0 per finalitzar): ");
                lletra = Console.ReadLine();
                paraula = paraula.Replace(lletra, null);

            } while (lletra != "0");

            Console.WriteLine("-----------------");
            Console.WriteLine("Paraula resultant: "+ paraula);
        }

        // Feu un programa que rebi per l’entrada 1 seqüència de caràcters i just després dos caràcters, el primer per substituir de l’String pel segon.
        public void SubstitueixElCaracter()
        {
            Console.Write("Escriu una paraula: ");
            string paraula = Console.ReadLine();

            Console.Write("Lletra substituida:    ");
            string lletra1 = Console.ReadLine();
            Console.Write("Lletra per substitiur: ");
            string lletra2 = Console.ReadLine();

            paraula = paraula.Replace(lletra1,lletra2);

            Console.WriteLine("-----------------");
            Console.WriteLine("Paraula resultant: " + paraula);
        }

        //Quan les cèl·lules es divideixen, el seu ADN també es replica.
        //De vegades durant aquest procés es produeixen errors i peces individuals d'ADN es codifiquen amb la informació incorrecta.
        //Si comparem dues cadenes d'ADN i comptem les diferències entre elles podem veure quants errors s'han produït.
        //Això es coneix com la "Distància d'Hamming".
        //Llegim l'ADN amb les lletres C,A,G i T

        public void DistanciaHamming()
        {
            Console.WriteLine("Introdueix dues cadenes d'ADN (a , c, g i t si o si):");

            string cadena1;
            string cadena2;
            do
            {
                Console.Write("La primera: ");
                cadena1 = Console.ReadLine();
                cadena1 = cadena1.ToUpper();
            } while (!cadena1.Contains("C") || !cadena1.Contains("A") || !cadena1.Contains("G") || !cadena1.Contains("T"));

            do
            {
                Console.Write("La segona:  ");
                cadena2 = Console.ReadLine();
                cadena2 = cadena2.ToUpper();
            } while (!cadena2.Contains("C") || !cadena2.Contains("A") || !cadena2.Contains("G") || !cadena2.Contains("T"));

            if (cadena1.Length == cadena2.Length)
            {
                int hamming = 0;

                for (int contador = 0; contador < cadena1.Length; contador++) //es com utilitzar vectors?
                {
                    string substring1 = cadena1.Substring(contador,1);
                    string subtring2 = cadena2.Substring(contador, 1);
                    hamming = hamming + Math.Abs(substring1.CompareTo(subtring2));
                }

                Console.WriteLine("La distància d'Hamming és: " + hamming );
            }
            else Console.WriteLine("Entrada no vàlida");
        }

        //Feu una aplicació que donada una seqüència amb només ‘(’ i ‘)’, digueu si els parèntesis tanquen correctament.
        //Entrada: L’entrada consisteix en una seqüència amb només ‘(’ i ‘)’.
        //Sortida: Per la sortida heu d’imprimir si els parèntesis tanquen bé o no.

        public void Parèntesis()
        {
            string sequencia;
            int stop = 0;

            do
            {
                Console.WriteLine("Inserta una seqüència de parèntesis:");
                sequencia = Console.ReadLine();
                stop = 0;

                for (int i = 0; i < sequencia.Length; i++)
                {
                    if (sequencia[i] == '(' || sequencia[i] == ')') stop++;
                }

            } while (stop != sequencia.Length);
            
            int contador = 0;

            for (int i = 0; i < sequencia.Length; i++)
            {
                if (sequencia[i] == '(')
                {
                    contador++;

                } else contador--;
            }

            if (contador != 0)
            {
                Console.WriteLine("Obert");

            } else Console.WriteLine("Tancat");
        }

        //Feu un programa que indiqui si una paraula és un palíndrom o no. Recordeu que una paraula és un palíndrom si es llegeix igual d’esquerra a dreta que de dreta a esquerra.
        //Entrada: L’entrada consisteix en un String.
        //Sortida: Per la sortida heu d’imprimir si la paraula és un palíndrom o no.
        public void Palíndrom()
        {
            Console.WriteLine("Inserta palíndrom:");
            string palindrom = Console.ReadLine();
            char[] invertit = palindrom.ToCharArray();

            Array.Reverse(invertit);

            string stringInvertit = new string(invertit);

            int resultat;
            resultat = palindrom.CompareTo(stringInvertit);

            if (resultat != 0)
            {
                Console.WriteLine("NO palíndrom");
            }
            else Console.WriteLine("Palíndrom");

        }

        // Feu un programa que llegeixi paraules, i que escrigui cadascuna invertint l’ordre dels seus caràcters.
        // Entrada: L’entrada consisteix en diverses paraules.
        // Sortida: Escriviu cada paraula, invertida, en una línia.
        public void InverteixLesParaules()
        {
            Console.WriteLine("Quantes paraules vols?");
            int vegades = Convert.ToInt32(Console.ReadLine());
            int stop = 0;
            do
            {
                Console.WriteLine("Inserta paraula:");
                string palindrom = Console.ReadLine();
                char[] invertit = palindrom.ToCharArray();

                Array.Reverse(invertit);
                Console.WriteLine("-----------");

                for (int i = 0; i < invertit.Length; i++)
                {
                    Console.Write(invertit[i]);
                }
                Console.WriteLine();
                Console.WriteLine("-----------");
                stop++;

            } while (stop != vegades);
        }

        //Feu un programa que llegeixi un String i inverteixi l’ordre de les paraules dins del mateix.
        //Entrada: L’entrada consisteix en un String.
        //Sortida: Escriviu l’String amb les seves paraules invertides.
        public void InverteixLesParaules2()
        {
            Console.WriteLine("Inserta un string:");
            string s = Console.ReadLine();

            string[] subs = s.Split(' ');

            Console.WriteLine("-----------");

            for (int i = subs.Length -1; i >= 0; i--)
            {
                Console.Write(subs[i]);
                Console.Write(" ");
            }
        }

        //Feu un programa que llegeixi una seqüència de lletres acabada en punt i digui si conté la successió de lletres consecutives ‘h’,‘o’, ‘l’, ‘a’ o no.
        //Entrada: L’entrada consisteix en una seqüència de lletres minúscules acabada en ‘.’.
        //Sortida: Cal escriure “hola” si l’entrada conté les lletres ‘h’, ‘o’, ‘l’, ‘a’ consecutivament.Altrament, cal escriure “adeu”.
        public void HolaIAdeu()
        {
            Console.WriteLine("Inserta una seqüència:");
            string sequencia = Console.ReadLine();
            string paraula = "hola";

            bool resultat = sequencia.Contains(paraula);

            Console.WriteLine("------");
            if (resultat == true)
            {
                Console.WriteLine("Hola");
            }
            else Console.WriteLine("Adeu");
        }

        public void Menu()
        {
            string opcio;
            do
            {
                Console.WriteLine("0.- FINALITZAR");
                Console.WriteLine("1.- SonIguals");
                Console.WriteLine("2.- SonIguals2");
                Console.WriteLine("3.- PurgaDeCaracters");
                Console.WriteLine("4.- SubstitueixElCaracter");
                Console.WriteLine("5.- DistanciaHamming");
                Console.WriteLine("6.- Parèntesis ");
                Console.WriteLine("7.- Palíndrom");
                Console.WriteLine("8.- InverteixLesParaules");
                Console.WriteLine("9.- InverteixLesParaules2");
                Console.WriteLine("A.- HolaIAdeu");
                
                opcio = Console.ReadLine();
                opcio = opcio.ToUpper();
                Console.Clear();
                switch (opcio)
                {
                    case "0":
                        Console.WriteLine("bye");
                        break;
                    case "1":
                        SonIguals();
                        break;
                    case "2":
                         SonIguals2();
                        break;
                    case "3":
                        PurgaDeCaracters();
                        break;
                    case "4":
                        SubstitueixElCaracter();
                        break;
                    case "5":
                        DistanciaHamming();
                        break;
                    case "6":
                        Parèntesis();
                        break;
                    case "7":
                        Palíndrom();
                        break;
                    case "8":
                        InverteixLesParaules();
                        break;
                    case "9":
                        InverteixLesParaules2();
                        break;
                    case "A":
                        HolaIAdeu();
                        break;
                    default:
                        Console.WriteLine("Opció Incorrecta");
                        break;
                }
                Console.ReadLine();
                Console.Clear();

            } while (opcio != "0");
        }

        public static void Main()
        {
            var menu = new Cadenes(); 
            menu.Menu();               
        }
    }
}
