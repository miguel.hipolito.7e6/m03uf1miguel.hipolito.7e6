﻿/*
 * AUTHOR: Miguel Á. Hipólito
 * DATE: 2022/12/1
 * DESCRIPTION: Corregir amb depurador els errors
 */

using System;

namespace IsAdult
{
    class IsAdult
    {
        public class Exercici1
        {
            static Boolean isAdult(int todayYear, int todayMonth, int todayDay, int birthYear,
                int birthMonth, int birthDay)
            {
                var isAdult = todayYear > birthYear + 18;
                var isDifficultYear = todayYear == birthYear + 18;
                var isAdultForMonth = isDifficultYear && todayMonth > birthMonth; //canvi
                var isDifficultMonth = isDifficultYear && todayMonth == birthMonth;
                var isAdultForDay = isDifficultMonth && todayDay >= birthDay; //canvi
                return isAdult || isAdultForMonth || isAdultForDay;
            }

            static void Main()
            {
                if (!isAdult(2018, 10, 17, 1985, 1, 1))
                {
                    Console.WriteLine("Ha fallat: isAdult(2018, 10, 17, 1985, 1, 1)");
                }
                if (isAdult(2018, 10, 17, 2015, 1, 1))
                {
                    Console.WriteLine("Ha fallat isAdult(2018, 10, 17, 2015, 1, 1)");
                }
                if (!isAdult(2018, 10, 17, 2000, 1, 1))
                {
                    Console.WriteLine("Ha fallat: isAdult(2018, 10, 17, 2000, 1, 1)");
                }
                if (isAdult(2018, 10, 17, 2000, 12, 1))
                {
                    Console.WriteLine("Ha fallat isAdult(2018, 10, 17, 2000, 12, 1)");
                }
                if (!isAdult(2018, 10, 17, 2000, 10, 1))
                {
                    Console.WriteLine("Ha fallat: isAdult(2018, 10, 17, 2000, 10, 1)");
                }
                if (!isAdult(2018, 10, 17, 2000, 10, 17))
                {
                    Console.WriteLine("Ha fallat: isAdult(2018, 10, 17, 2000, 10, 17)");
                }
                if (isAdult(2018, 10, 17, 2000, 10, 30))
                {
                    Console.WriteLine("Ha fallat isAdult(2018, 10, 17, 2000, 10, 30)");
                }

            }
        }
    }
}
