﻿/*
 *AUTHOR: Miguel Á.Hipólito
 * DATE: 2022/12/1
 * DESCRIPTION: Corregir amb depurador els errors
*/

using System;

namespace Mates
{
    class Mates
    {
        static void Main()
        {
            var result = 0.0;
            for (int i = 0; i < 10000; i++)
            {
                if (result > 100)
                {
                    result = Math.Sqrt(result);
                }

                result += 20.2;
            }

            Console.WriteLine($"el resultat és {result}");

            //Quin valor té result quan la i == 1000? 111,56230589874906
            //El valor result és mai major que 110? I que 120? Result será major que 110 Exemple: i == 10.
            //En canvi, amb 120 mai ho supera.

        }
    }
}
