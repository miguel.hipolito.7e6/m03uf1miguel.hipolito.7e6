﻿/*
 *AUTHOR: Miguel Á.Hipólito
 * DATE: 2022/12/1
 * DESCRIPTION: Corregir amb depurador els errors
*/

using System;

namespace TeamColor
{
    class TeamColor
    {
        static void Main()
        {
            string color1 = Console.ReadLine();
            string color2 = Console.ReadLine();

            if (color1 == "white")
            {
                if (color2 == "green")
                {
                    Console.WriteLine("Team1");
                }
                else if (color2 == "blue")
                {
                    Console.WriteLine("Team2");
                }
                else if (color2 == "brown")
                {
                    Console.WriteLine("Team3");

                } else Console.WriteLine("ERROR");
            }
            else if (color1 == "red")
            {
                if (color2 == "blue")
                {
                    Console.WriteLine("Team4");
                }
                else if (color2 == "black")
                {
                    Console.WriteLine("Team5");

                } else Console.WriteLine("ERROR");
            }
            else if (color1 == "green")
            {
                if (color2 == "red")
                {
                    Console.WriteLine("Team6");

                } else Console.WriteLine("ERROR");
            }
            else Console.WriteLine("ERROR");
        }
    }
}
