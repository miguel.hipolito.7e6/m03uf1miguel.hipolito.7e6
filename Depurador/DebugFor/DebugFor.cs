﻿/*
 *AUTHOR: Miguel Á.Hipólito
 * DATE: 2022/12/1
 * DESCRIPTION: Corregir amb depurador els errors
*/
using System;

namespace DebugFor
{
    class DebugFor
    {
        static void Main()
        {
            double numero1 = 548745184; 
            double numero2 = 25145;
            double result = 0;

            for (int i = 0; i < numero2; i++)
            {
                result += numero1;
            }

            Console.WriteLine("la multiplicació de {0} i {1} es {2}", numero1, numero2, result);
        }
    }
}
