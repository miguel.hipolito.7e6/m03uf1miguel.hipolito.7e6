﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/14
* DESCRIPTION: L'usuari escriu un enter amb la seva edat i s'imprimeix true si és major d'edat, i false en qualsevol
altre cas.
*/

using System;

namespace _1._17
{
    class Program
    {
        static void Main(string[] args)
        {
            int edat = 0;
            int A = 18;

            Console.WriteLine("Escriu un nombre enter");
            edat = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(edat >= A);
        }
    }
}
