﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/15
* DESCRIPTION: Fes un programa que, rebuts dos nombres per l’entrada, comprovi si el primer és divisible pel
segon.
*/

using System;

namespace _1._33_És_divisible
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inserta el divisor i el divident:");
            int divisor = Convert.ToInt32(Console.ReadLine());
            int divident = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(divident % divisor == 0);
        }
    }
}
