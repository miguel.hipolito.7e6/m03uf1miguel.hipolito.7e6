﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/16
* DESCRIPTION:  Printa el valor absolut d'un enter entrat per l'usuari.
*/

using System;

namespace EX1.AbsoluteNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            int A = 0;
            Console.WriteLine("Escriu un enter");
            A = Convert.ToInt32(Console.ReadLine());

            if (A >= 0)
            {
                Console.WriteLine(A);
            }
            else
            {
                Console.WriteLine(Math.Abs(A));
            }

        }
    }
}
