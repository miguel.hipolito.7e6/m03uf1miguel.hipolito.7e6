﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/15
* DESCRIPTION: Després de molt d'esforç hem aconseguit dissenyar una màquina del temps. Només ens falta una
utilitat, necessitem un petit programa que ens indiqui en quin dia estem. Fes un programa que
imprimeixi per pantalla el següent missatge Avui és: 2021-09-17.
*/

using System;

namespace _1._37_Ajuda_per_la_màquina_de_viatge_en_el_temps
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime localDate = DateTime.Now;
            Console.WriteLine("Avui és "+ localDate.ToString("yyyy-MM-dd"));
        }
    }
}
