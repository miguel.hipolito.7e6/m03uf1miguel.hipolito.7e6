﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/13
* DESCRIPTION: Per poder fer un estudi de la ventilació d'una aula necessitem poder calcular la quantitat d'aire
que hi cap en una habitació. Llegeix les 3 dimensions de l'aula i imprimeix per pantalla quin volum
té.
*/

using System;

namespace _1._11
{
    class Program
    {
        static void Main(string[] args)
        {
            double A = 0;
            double B = 0;
            double C = 0;

            Console.WriteLine("Afageix tres nombres per calcular el volum");
            A = Convert.ToDouble(Console.ReadLine());
            B = Convert.ToDouble(Console.ReadLine());
            C = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine(A * B * C);
        }
    }
}
