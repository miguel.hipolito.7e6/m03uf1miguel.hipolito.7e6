﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/15
* DESCRIPTION: Escriu un programa que llegeixi 3 frases diferents per terminal i en faci 1 història
*/

using System;

namespace _1._36_Construeix_la_història
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inserta tres frases:");
            string frase1 = Convert.ToString(Console.ReadLine());
            string frase2 = Convert.ToString(Console.ReadLine());
            string frase3 = Convert.ToString(Console.ReadLine());

            Console.WriteLine(frase1 + " " + frase2 + " " + frase3);
        }
    }
}
