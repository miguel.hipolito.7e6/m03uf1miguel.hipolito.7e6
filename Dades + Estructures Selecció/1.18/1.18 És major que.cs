﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/14
* DESCRIPTION: Fes un programa que rebi dos nombres enters i imprimeixi true si el primer és major que el
segon, false en cap altre cas.
*/

using System;

namespace _1._18
{
    class Program
    {
        static void Main(string[] args)
        {
            int A = 0;
            int B = 0;

            Console.WriteLine("Escriu un nombre A i B, per veure si A es major que B");
            A = Convert.ToInt32(Console.ReadLine());
            B = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(A > B);
        }
    }
}
