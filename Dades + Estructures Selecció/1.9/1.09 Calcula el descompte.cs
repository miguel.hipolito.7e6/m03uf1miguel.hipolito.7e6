﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/13
* DESCRIPTION: Llegeix el preu original i el preu actual i imprimeix el descompte (en %).
*/

using System;

namespace _1._9
{
    class Program
    {
        static void Main(string[] args)
        {
            double preuoriginal = 0;
            double preuactual = 0;

            Console.WriteLine("inserta Preu original i el Preu actual respectivament");
            preuoriginal = Convert.ToInt32(Console.ReadLine());
            preuactual = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine((1 - (preuactual / preuoriginal)) * 100);
        }
    }
}
