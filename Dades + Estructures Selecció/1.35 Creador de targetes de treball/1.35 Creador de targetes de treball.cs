﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/15
* DESCRIPTION: Escriu un programa que imprimeixi targetes de treball. Aquestes han de contenir, nom, cognom i
número de despatx de la persona treballadora.
*/

using System;

namespace _1._35_Creador_de_targetes_de_treball
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inserta el  teu nombre sencer i el número de despatx:");
            string nom = Convert.ToString(Console.ReadLine());
            string despatx = Convert.ToString(Console.ReadLine());

            Console.WriteLine("Empleada: "+ nom + " - Despatx: " + despatx);
        }
    }
}
