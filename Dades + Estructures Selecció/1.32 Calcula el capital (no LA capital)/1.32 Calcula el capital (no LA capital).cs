﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/15
* DESCRIPTION: Fes un programa que demani un capital, i calculi quin capital es generarà passat cert temps
segons certs interessos (entre 0% i 100%).
*/

using System;

namespace _1._32_Calcula_el_capital__no_LA_capital_
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Tinc en compte un interés simple i no compost*/
            Console.WriteLine("Inserta el capital, nombre d'anys i percentatge:");
            double capital = Convert.ToInt32(Console.ReadLine());
            double anys = Convert.ToInt32(Console.ReadLine());
            double percentatge = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(capital * (1 + (anys * percentatge / 100)));
        }
    }
}
