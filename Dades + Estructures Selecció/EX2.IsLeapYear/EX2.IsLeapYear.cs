﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/20
* DESCRIPTION: L'usuari introdueix un any. Indica si és de traspàs printant "2020 és any de traspàs" o "2021 no és any de traspàs".
*/

using System;

namespace EX2.IsLeapYear
{
    class Program
    {
        static void Main(string[] args)
        {
            double any = 0;

            Console.WriteLine("Escriu el teu any");
            any = Convert.ToDouble(Console.ReadLine());

            if (any % 4 == 0)
            {
                if (any % 100 == 0)
                {
                    if (any % 400 == 0)   
                    {
                        Console.WriteLine("Es bisiesto");  // si es divisible entre 4, 100 y 100, es bisiesto
                    } else
                    {
                        Console.WriteLine(" NO es bisiesto"); //si es divisible entre 4 y 100, no es
                    }
                } else 
                {
                    Console.WriteLine("Es bisiesto");  //si es divisible entre 4, es bisiesto
                }
            } else
            {
                Console.WriteLine(" NO es bisiesto"); //si no es divisble entre 4, no lo es
            }
        }
    }
}
