﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/14
* DESCRIPTION: Fes un programa que rebi dos nombres decimals i torni si són iguals o no
*/

using System;

namespace _1._20
{
    class Program
    {
        static void Main(string[] args)
        {
            double A = 0;
            double B = 0;

            Console.WriteLine("Escriu un decimal A i B, per veure si A es igual que B");
            A = Convert.ToDouble(Console.ReadLine());
            B = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine(A == B);
        }
    }
}
