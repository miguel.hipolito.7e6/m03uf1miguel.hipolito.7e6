﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/14
* DESCRIPTION: Fes un programa que rebi un caràcter i digui si és un nombre o no.
*/

using System;

namespace _1._24_És_un_nombre
{
    class Program
    {
        static void Main(string[] args)
        {
            bool resultat;
            Console.WriteLine("Inserta un nombre:");
            char a = Convert.ToChar(Console.ReadLine());

            resultat = Char.IsDigit(a);
            Console.WriteLine(resultat);
        }
    }
}
