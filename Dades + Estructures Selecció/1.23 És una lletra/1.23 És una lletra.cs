﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/14
* DESCRIPTION: Fes un programa que rebi un caràcter i digui si és una lletra o no.
*/

using System;

namespace _1._23_És_una_lletra
{
    class Program
    {
        static void Main(string[] args)
        {
            bool resultat;
            Console.WriteLine("Inserta un caracter:");
            char a = Convert.ToChar(Console.ReadLine());

            resultat = Char.IsLetter(a);
            Console.WriteLine(resultat);
        }
    }
}
