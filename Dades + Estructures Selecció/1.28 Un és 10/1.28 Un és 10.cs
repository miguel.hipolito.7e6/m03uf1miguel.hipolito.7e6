﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/14
* DESCRIPTION: Fes un programa que donats 4 enters ens diguis si hi ha cap que sigui 10.
*/

using System;

namespace _1._28_Un_és_10
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inserta 4 nombres:");

            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            int c = Convert.ToInt32(Console.ReadLine());
            int d = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(a == 10 || b == 10 || c == 10 || d == 10);
        }
    }
}
