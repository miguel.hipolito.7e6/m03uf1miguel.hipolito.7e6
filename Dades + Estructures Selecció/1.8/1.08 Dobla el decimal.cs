﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/13
* DESCRIPTION: Llegeix un valor amb decimals i imprimeix el doble.
*/

using System;

namespace _1._8
{
    class Program
    {
        static void Main(string[] args)
        {
            double A = 0;

            Console.WriteLine("escriu un nombre decimal");
            A = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine(A * 2);
        }
    }
}
