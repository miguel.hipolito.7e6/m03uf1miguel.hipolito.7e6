﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/14
* DESCRIPTION: Fes un programa que rebi un valor booleà i et retorni el valor contrari
*/

using System;

namespace _1._19
{
    class Program
    {
        static void Main(string[] args)
        {
            Boolean A = true;

            Console.WriteLine("Escriu true or false");
            A = Convert.ToBoolean(Console.ReadLine());

            Console.WriteLine(!A);
        }
    }
}
