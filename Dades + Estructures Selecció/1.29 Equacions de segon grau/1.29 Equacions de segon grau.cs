﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/15
* DESCRIPTION: Fes un programa que resolgui la fórmula d’una equació de segon grau
*/

using System;

namespace _1._29_Equacions_de_segon_grau
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inserta a, b i c:");

            int a = Convert.ToInt32(Console.ReadLine());
            int b = Convert.ToInt32(Console.ReadLine());
            int c = Convert.ToInt32(Console.ReadLine());

            double resultatpos = (-b + Math.Sqrt(Math.Pow(b,2) - 4 * a * c)) / (2 * a);
            double resultatneg = (-b - Math.Sqrt(Math.Pow(b, 2) - 4 * a * c)) / (2 * a);

            Console.WriteLine("---------");
            Console.WriteLine(resultatpos);
            Console.WriteLine(resultatneg);
        }
    }
}
