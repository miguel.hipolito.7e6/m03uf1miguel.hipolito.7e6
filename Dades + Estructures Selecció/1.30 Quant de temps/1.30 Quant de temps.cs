﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/15
* DESCRIPTION: Fes un programa que calculi quants minuts, hores i segons hi ha en un número de segons
donats.
*/

using System;

namespace _1._30_Quant_de_temps
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inserta nombre de segons:");
            int nsegons = Convert.ToInt32(Console.ReadLine());

            int segons = nsegons % 60;
            int minuts = nsegons / 60 % 60;
            int hores = nsegons / 60 / 60;

            Console.WriteLine(hores + " hora " + minuts + " minuts " + segons + " segons");
        }
    }
}
