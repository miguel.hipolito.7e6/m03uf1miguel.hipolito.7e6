﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/15
* DESCRIPTION: Escriu un programa que imprimeixi targetes de treball. Aquestes han de contenir, nom, cognom i
número de despatx de la persona treballadora.
*/

using System;

namespace _1._34_Hola_Usuari
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inserta el teu nom:");
            string nom = Convert.ToString(Console.ReadLine());

            Console.WriteLine("Bon dia "+ nom);
        }
    }
}
