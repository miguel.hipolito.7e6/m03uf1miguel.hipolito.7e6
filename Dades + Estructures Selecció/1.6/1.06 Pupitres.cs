﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/13
* DESCRIPTION: En una escola tenim tres classes i volem saber quin és el nombre de taules que necessitarem
tenir en total. Dependrà del nombre d'alumnes per aula. Cal tenir en compte que a cada taula hi
caben 2 alumnes.
*/

using System;

namespace _1._6
{
    class Program
    {
        static void Main(string[] args)
        {
            int A = 0;
            int At = 0;
            int B = 0;
            int Bt = 0;
            int C = 0;
            int Ct = 0;

            Console.WriteLine("Escriu nºalumnes de la classe A, B y C");
            A = Convert.ToInt32(Console.ReadLine());
            At = (A % 2 + A / 2);  // 22%2 + 22/2 = 11

            /* (antigua forma)
            At = A
            At = At % 2;  22 = 22 % 2 = 0
            A = A / 2;      22 = 22 / 2 = 11
            A = At + A;     11 = 0 + 11 = 11
            */

            B = Convert.ToInt32(Console.ReadLine());
            Bt = (B % 2 + B / 2);

            C = Convert.ToInt32(Console.ReadLine());
            Ct = (C % 2 + C / 2);

            Console.WriteLine(At + Bt + Ct);
        }
    }
}
