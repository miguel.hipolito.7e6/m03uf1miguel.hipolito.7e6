﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/13
* DESCRIPTION: Fes un programa on, introduit el número de començals i el preu d'un sopar (que pot contenir
cèntims), imprimeixi quan haurà de pagar cada començal.
*/

using System;

namespace _1._13
{
    class Program
    {
        static void Main(string[] args)
        {
            double temperatura = 0;
            double increment = 0;

            Console.WriteLine("Escriu la temperatura i el increment");
            temperatura = Convert.ToDouble(Console.ReadLine());
            increment = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine($"la temperatura actual es: {temperatura + increment}º");
        }
    }
}
