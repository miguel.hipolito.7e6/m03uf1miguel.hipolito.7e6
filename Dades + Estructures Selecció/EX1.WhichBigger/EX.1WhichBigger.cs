﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/16
* DESCRIPTION: Demana dos enters a l'usuari i imprimeix el valor més gran
*/

using System;

namespace EX1.WhichBigger
{
    class Program
    {
        static void Main(string[] args)
        {
            int A = 0;
            int B = 0;

            Console.WriteLine("Escriu dos enters (A i B) i et dic quin és més gran");
            A = Convert.ToInt32(Console.ReadLine());
            B = Convert.ToInt32(Console.ReadLine());

            if (A > B)
            {
                Console.WriteLine("A es major que B");
            }
            else
            {
                Console.WriteLine("B es major que A");
            }
        }
    }
}
