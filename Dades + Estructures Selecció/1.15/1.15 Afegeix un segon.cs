﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/13
* DESCRIPTION: Fes un programa que afegeixi 1 segon un nombre de segons determinat.
*/

using System;

namespace _1._15
{
    class Program
    {
        static void Main(string[] args)
        {
            int segon = 0;
       
            Console.WriteLine("Escriu un nombre entre 0 i 60");
            segon = Convert.ToInt32(Console.ReadLine());
            segon = (segon + 1) % 60;      //40 + 1 = 41    41 % 60 = 41

            Console.WriteLine(segon);
        }
    }
}
