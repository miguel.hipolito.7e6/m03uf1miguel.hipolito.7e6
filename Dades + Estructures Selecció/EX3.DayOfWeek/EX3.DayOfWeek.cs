﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/20
* DESCRIPTION: L'usuari demana una número de l'1 al 7. Per consola s'imprimerix:  Dilluns, Dimarts, Dimecres, Dijous,...
*/

using System;

namespace EX3.DayOfWeek
{
    class Program
    {
        static void Main(string[] args)
        {
            int dia = 0;

            Console.WriteLine("Escriu el numero de dia");
            dia = Convert.ToInt32(Console.ReadLine());

            switch (dia)
            {
                case 1:
                    Console.WriteLine("Es Lunes");
                    break;
                case 2:
                    Console.WriteLine("Es Martes");
                    break;
                case 3:
                    Console.WriteLine("Es Miercoles");
                    break;
                case 4:
                    Console.WriteLine("Es Jueves");
                    break;
                case 5:
                    Console.WriteLine("Es Viernes");
                    break;
                case 6:
                    Console.WriteLine("Es Sabado");
                    break;
                case 7:
                    Console.WriteLine("Es Domingo");
                    break;
            }
        }
    }
}
