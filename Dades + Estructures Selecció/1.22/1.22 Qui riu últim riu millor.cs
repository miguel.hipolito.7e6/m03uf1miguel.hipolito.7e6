﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/14
* DESCRIPTION: Fes un programa que rebi el valor del nivell de riure de 5 persones i comprovi si la dita és certa.
*/

using System;

namespace _1._22
{
    class Program
    {
        static void Main(string[] args)
        {
            int A = 0;
            int B = 0;
            int C = 0;
            int D = 0;

            Console.WriteLine("Escriu un enter A, B, C i D, per veure si D es major que la resta");
            A = Convert.ToInt32(Console.ReadLine());
            B = Convert.ToInt32(Console.ReadLine());
            C = Convert.ToInt32(Console.ReadLine());
            D = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(D > C || D > B || D > A);
        }
    }
}