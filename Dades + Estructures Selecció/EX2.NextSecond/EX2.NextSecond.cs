﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/20
* DESCRIPTION: L'usuari introdueix una hora amb tres enters (hores, minuts i segons). Imprimeix l'hora que serà al cap d'un segon
*/

using System;

namespace NextSecond
{
    class Program
    {
        static void Main(string[] args)
        {
            double hora = 0;
            double minuts = 0;
            double segons = 0;

            Console.WriteLine("Escriu la teva hora");
            hora = Convert.ToDouble(Console.ReadLine());
            minuts = Convert.ToDouble(Console.ReadLine());
            segons = Convert.ToDouble(Console.ReadLine());

            segons = segons + 1; 

            if (segons % 60 == 0)
            {
                segons = 0;
                minuts = minuts + 1;
            }
            if (minuts % 60 == 0)
            {
                minuts = 0;
                hora = hora + 1;
            }
            if (hora % 24 == 0)
            {
                hora = 0;
                minuts = 0;
                segons = 0;
            }

            Console.WriteLine($"{hora}:{minuts}:{segons}");
        }
    }
}
