﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/16
* DESCRIPTION: Introdueix el número de persones i el número de galetes. Si a tothom li toquen el mateix número de galetes imprimeix "Let's Eat!", sinó imprimeix "Let's Fight"
*/

using System;

namespace EX1.WillWeFightForThecookies
{
    class Program
    {
        static void Main(string[] args)
        {
            int persones = 0;
            int galetes = 0;

            Console.WriteLine("Escriu el nºpersones i el nºgaletes respectivament");
            persones = Convert.ToInt32(Console.ReadLine());
            galetes = Convert.ToInt32(Console.ReadLine());

            if (galetes == persones)
            {
                Console.WriteLine("Let's Eat!");
            }
            else
            {
                Console.WriteLine("Let's Fight");

            }
        }
    }
}
