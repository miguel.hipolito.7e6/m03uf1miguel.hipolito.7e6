﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/15
* DESCRIPTION: Fes un programa que demani un capital, i calculi quin capital es generarà passat cert temps
segons certs interessos (entre 0% i 100%).
*/

using System;

namespace _1._31_De_metre_a_peu
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inserta nombre de metres:");
            int metres = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(metres * 39.37 / 12);
        }
    }
}
