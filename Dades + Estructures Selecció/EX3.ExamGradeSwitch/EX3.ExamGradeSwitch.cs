﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/20
* DESCRIPTION: L'usuari escriu un valor que representa una nota. Imprimeix "Excelent", "Notable", "Bé", "Suficient", "Suspès", "Nota invàlida" segons  la nota numèrica introduïda
*/

using System;

namespace EX3.ExamGradeSwitch
{
    class Program
    {
        static void Main(string[] args)
        {
            double nota = 0;

            Console.WriteLine("Escriu la teva nota");
            nota = Convert.ToDouble(Console.ReadLine());

            //versión no acepta intervalos, da igual...
            
            switch (nota)
            {
                case < 4.4:
                    Console.WriteLine("Suspès");
                    break;
                case <= 5.4:
                    Console.WriteLine("Suficient");
                    break;
                case <= 6.4:
                    Console.WriteLine("Bé");
                    break;
                case <=8.4:
                    Console.WriteLine("Notable");
                    break;
                case <=10:
                    Console.WriteLine("Excelent");
                    break;
            }
        }
    }
}
