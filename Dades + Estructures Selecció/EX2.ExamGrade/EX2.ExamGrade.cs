﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/20
* DESCRIPTION: L'usuari escriu un valor que representa una nota. Imprimeix "Excelent", "Notable", "Bé", "Suficient", "Suspès", "Nota invàlida" segons el la nota numèrica introduïda
*/

using System;

namespace EX2.ExamGrade
{
    class Program
    {
        static void Main(string[] args)
        {
            double nota = 0;

            Console.WriteLine("Escriu la teva nota");
            nota = Convert.ToDouble(Console.ReadLine());

            if (nota < 4.5 ) { 
                Console.WriteLine("Suspès");
            } else if (nota >= 4.5 && nota < 5.5 ) {
                Console.WriteLine("Suficient");
            } else if (nota >= 5.5 && nota < 6.5) {
                Console.WriteLine("Bé");
            } else if (nota >= 6.5 && nota < 8.5) {
                Console.WriteLine("Notable");
            } else {
                Console.WriteLine("Excelent");
            }
        }
    }
}
