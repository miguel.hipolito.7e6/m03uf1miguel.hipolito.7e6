﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/16
* DESCRIPTION: L'usuari escriu un enter. Imprimeix  "bitllet vàlid"  si existeix un bitllet d'euros amb la quantitat entrada, "bitllet invàlid" en qualsevol altre cas.
*/

using System;

namespace EX1.NiceIsValidNote
{
    class Program
    {
        static void Main(string[] args)
        {
            int billete = 0;
            Console.WriteLine("Inserta el teu bitllet");
            billete = Convert.ToInt32(Console.ReadLine());

            if (billete == 5 || billete == 10 || billete == 20 || billete == 50 || billete == 100 || billete == 500)
            {
                Console.WriteLine("Bitllet valid");
            }
            else
            {
                Console.WriteLine("Bitllet invalid");
            }
        }
    }
}
