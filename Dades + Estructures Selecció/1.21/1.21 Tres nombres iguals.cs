﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/14
* DESCRIPTION: Fes un programa que rebi tres nombres enters i torni si són iguals o no
*/

using System;

namespace _1._21
{
    class Program
    {
        static void Main(string[] args)
        {
            int A = 0;
            int B = 0;
            int C = 0;

            Console.WriteLine("Escriu un decimal A, B i C, per veure si A es igual que B");
            A = Convert.ToInt32(Console.ReadLine());
            B = Convert.ToInt32(Console.ReadLine());
            C = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(A == B && A == C);
        }
    }
}
