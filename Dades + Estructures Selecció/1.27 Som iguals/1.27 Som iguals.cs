﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/14
* DESCRIPTION: Fes un programa que rebi un caràcter de l’abecedari en minúscula i un altre en majúscula i digui
si són corresponents.
*/

using System;

namespace _1._27_Som_iguals
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inserta dues lletra:");
            char a = Convert.ToChar(Console.ReadLine());
            char b = Convert.ToChar(Console.ReadLine());

            Console.WriteLine(char.ToLower(a) == char.ToLower(b));
        }
    }
}
