﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/16
* DESCRIPTION:  Volem comparar quina pizza és més gran, entre una rectangular i una rodona
	- L'usuari entra el diametre d'una pizza rodona
	- L'usuari entra els dos costats de la pizza rectangular
	- Imprimeix "Compra la rodona" si la pizza rodona és més gran, o "Compea la rectangular" en qualsevol altre cas.
*/

using System;

namespace EX1.WhichPizzaShouldIBuy
{
    class Program
    {
        static void Main(string[] args)
        {
            double diametre = 0;
            double costatA = 0;
            double costatB = 0;

            Console.WriteLine("Volem comparar quina pizza es més gran, si una rodona o redctangular. Escriu primer el diametre de la rodona");
            diametre = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Ara escriu els costats de la rectangular");
            costatA = Convert.ToDouble(Console.ReadLine());
            costatB = Convert.ToDouble(Console.ReadLine());

            double rodona = Math.PI * Math.Pow(diametre / 2, 2);
            double rectangular = costatA * costatB;

            if (rodona > rectangular)
            {
                Console.WriteLine("Compra la Rodona");
            }
            else
            {
                Console.WriteLine("Compra la Rectangular");
            }
        }
    }
}
