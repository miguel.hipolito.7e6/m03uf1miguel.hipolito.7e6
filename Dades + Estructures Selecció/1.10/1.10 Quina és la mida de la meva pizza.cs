﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/13
* DESCRIPTION: Llegeix el diàmetre d'una pizza rodona i imprimeix la seva superfície. Pots usar Math.PI per
escriure el valor de Pi.
*/

using System;

namespace _1._10
{
    class Program
    {
        static void Main(string[] args)
        {
            double diametre = 0;

            Console.WriteLine("Inserta diametre pizza:");
            diametre = Convert.ToDouble(Console.ReadLine());

            Console.WriteLine($"La superficie es: {Math.Pow(diametre/2, 2) * Math.PI}");
        }
    }
}
