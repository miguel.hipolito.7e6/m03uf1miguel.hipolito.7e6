﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/14
* DESCRIPTION: Fes un programa que rebi un caràcter de l’abecedari en majúscula i el faci minúscula.
*/

using System;

namespace _1._26_Fes_me_minúscula
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inserta una lletra:");
            char a = Convert.ToChar(Console.ReadLine());

            Console.WriteLine(char.ToLower(a));
        }
    }
}
