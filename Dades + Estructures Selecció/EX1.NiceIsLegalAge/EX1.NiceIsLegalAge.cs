﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/16
* DESCRIPTION:  Demana l'edat de l'usuari.  Printa  "Ets major d'edat", si és major d'edat.
*/

using System;

namespace EX1.NiceIsLegalAge
{
    class Program
    {
        static void Main(string[] args)
        {
            int A = 0;
            Console.WriteLine("Escriu la teva edat");
            A = Convert.ToInt32(Console.ReadLine());

            if (A >= 18) Console.WriteLine("Ets major d'edat");

        }
    }
}
