﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/13
* DESCRIPTION: Escriu un programa que llegeixi un nombre enter i imprimeixi una frase amb el següent nombre
enter.
*/

using System;

namespace _1._7
{
    class Program
    {
        static void Main(string[] args)
        {
            int A = 0;

            Console.WriteLine("Escriu un nombre enter");
            A = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine(A+1);
        }
    }
}
