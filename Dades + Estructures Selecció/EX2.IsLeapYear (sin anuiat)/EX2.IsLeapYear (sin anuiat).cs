﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/20
* DESCRIPTION: L'usuari introdueix un any. Indica si és de traspàs printant "2020 és any de traspàs" o "2021 no és any de traspàs".
*/

using System;

namespace EX2.IsLeapYear__sin_anuiat_
{
    class Program
    {
        static void Main(string[] args)
        {
            double any = 0;

            Console.WriteLine("Escriu el teu any");
            any = Convert.ToDouble(Console.ReadLine());

            if (any % 4 == 0 && !(any % 100==0) || any%400==0)
            {
                Console.WriteLine("Es bisiesto");
            }
            else
            {
                Console.WriteLine(" NO es bisiesto");
            }
        }
    }
}
