﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/09/14
* DESCRIPTION: Fes un programa que rebi un caràcter de l’abecedari en minúscula i el faci majúscula.
*/

using System;

namespace _1._25_Fes_me_majúscula
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inserta una lletra:");
            char a = Convert.ToChar(Console.ReadLine());

            Console.WriteLine(char.ToUpper(a));
        }
    }
}
