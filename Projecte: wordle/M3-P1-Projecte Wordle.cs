﻿/*
 * AUTHOR: Miguel Á. Hipólito
 * DATE: 2022/11/16
 * DESCRIPTION: Projecte Wordle
 */

using System;

namespace M3_P1_Projecte_Wordle
{
    class Program
    {
        static void Main(string[] args)
        {
            string stop;
            int ganar = 0; 

            /* palabra para comprobar
            string palabra = "joven";
            Console.WriteLine(palabra);*/

            Console.WriteLine("BIENVENIDO A MI WORDLE (tm)!!"); 
            do
            {
                Console.WriteLine("Para jugar una partida dale a ENTER o esribe END:");
                stop = Console.ReadLine();

                if (stop != "END")
                {
                    ganar = 0;

                    string[] respositorio = { "pared", "limon", "saber", "timon", "botas", "joven", "viejo", "gatos", "tigre", "movil", "agrio", "gotas", "pared", "ajeno", "feliz", "agrio", "asilo", "razon", "labor", "hedor" };
                    Random rnd = new Random();
                    string palabra = respositorio[rnd.Next(0, 19 + 1)];

                    for (int nIntentos = 6; nIntentos > 0 && ganar != 5; nIntentos--) /*comprobar intentos */
                    {
                        ganar = 0;

                        Console.WriteLine("TIENES [" + nIntentos + "] INTENTOS PARA ADIVINAR LA PALABRA:");
                        string intento = Console.ReadLine();

                        while (intento.Length != 5) /*comprobar caracteres */
                        {
                            Console.WriteLine("-------------------------");
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("HA DE TENER 5 CARÁCTERES:");
                            Console.ResetColor();
                            intento = Console.ReadLine();
                        }

                        Console.WriteLine("---------------");

                        for (int indexIntento = 0; indexIntento < intento.Length && ganar != 5; indexIntento++) /* comprovar letras*/
                        {
                            int posicio = 0;

                            for (int indexP = 0; indexP < palabra.Length; indexP++)
                            {
                                if (intento[indexIntento] == palabra[indexP])
                                {
                                    if (indexIntento == posicio) /* letra correcta y mateiza posición*/
                                    {
                                        Console.BackgroundColor = ConsoleColor.Green;
                                        Console.Write("[" + palabra[indexP] + "]");
                                        Console.ResetColor();
                                        ganar++;

                                    }
                                    else                        /* letra correcta */
                                    {
                                        Console.BackgroundColor = ConsoleColor.Yellow;
                                        Console.Write("[" + palabra[indexP] + "]");
                                        Console.ResetColor();
                                    }
                                    posicio--;
                                }
                                else if (posicio == 4) Console.Write("[" + intento[indexIntento] + "]"); /*letra incorrecta*/
                                posicio++;
                            }
                        }
                        Console.WriteLine();
                        Console.WriteLine("---------------");
                    }
                    if (ganar == 5)   /*condicion ganar */
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("HAS GANADO");
                        Console.ResetColor();
                    }
                    else              /*condicion perder */
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("HAS PERDIDO");
                        Console.ResetColor();

                        Console.WriteLine("La palabra era: " + palabra);
                    }
                    Console.WriteLine("");
                }

            } while (stop != "END");
        }
    }
}
