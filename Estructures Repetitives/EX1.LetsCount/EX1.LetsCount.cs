﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/04
* DESCRIPTION: Mostra per pantalla tots els números fins a un enter entrat per l'usuari.
*/

using System;

namespace EX1.LetsCount
{
    class Program
    {
        static void Main(string[] args)
        {
            int contador = 1;
            int numerolimite = 0;

            Console.WriteLine("Inserta el limite");
            numerolimite = Convert.ToInt32(Console.ReadLine());

            while (contador <= numerolimite)
            {
                Console.Write(contador);
                contador++;
            }
        }
    }
}
