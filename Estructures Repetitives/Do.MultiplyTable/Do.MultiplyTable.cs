﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/5
* DESCRIPTION: Volem printar les taules de multiplicar.
*/

using System;

namespace Do.MultiplyTable
{
    class Program
    {
        static void Main(string[] args)
        {
            int Tabla = 0;
            int limit = 1;

            Console.WriteLine("Inserta el numero");
            Tabla = Convert.ToInt32(Console.ReadLine());

            do
            {
                Console.WriteLine($"{limit}*{Tabla}= {limit * Tabla}");
                limit++;
            } while (limit < 10);
        }
    }
}
