﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/19
* DESCRIPTION: S'imprimeix una piràmide d'altura N de # centrada
*/

using System;
namespace AV.PiramidCenter
{
    class Program
    {
        static void Main(string[] args)
        {
            int nombre;

            Console.WriteLine("Inserte un nombre:");
            nombre = Convert.ToInt32(Console.ReadLine());

            for (int a = 1; a <= nombre; a++)
            {
                int espacios = nombre - a;
                for (int b = 1; b <= a; b++)
                {
                    for (; espacios > 0; espacios--) //al salir espacios, vale 0. Hay que evitarlo
                    {
                        Console.Write(" ");
                    }
                    Console.Write("# ");
                }
                Console.WriteLine();
            }
        }
    }
}
