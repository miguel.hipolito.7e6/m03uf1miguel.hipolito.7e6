﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/11
* DESCRIPTION: Demana un enter a l'usuari. Imprimeix per pantalla tants punts com l'usuari hagi indicat
*/

using System;

namespace EX2.DotLine
{
    class Program
    {
        static void Main(string[] args)
        {
            int limite;

            Console.WriteLine("Inserta el nº de punts");
            limite = Convert.ToInt32(Console.ReadLine());

            for (int a = 1 ;a <= limite; a++)
            {
                Console.Write(".");
            }
        }
    }
}
