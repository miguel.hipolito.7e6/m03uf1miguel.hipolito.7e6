﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/04
* DESCRIPTION: Volem comptar quantes línies té un text introduït per l'usuari.
*/

using System;

namespace EX1.HowManyLines
{
    class Program
    {
        static void Main(string[] args)
        {
            int linias = 0;
            string END = "a";

            Console.WriteLine("Escriu una linia");
            END = Console.ReadLine();

            while (END != "END")
            {
                Console.WriteLine("Escriu una linia");
                END = Console.ReadLine();
                linias++;
            }
            Console.WriteLine(linias);
        } //{end no cuenta, por ello, he restado 1 y ya} MAL, mejor utilizar esto 
    }
}