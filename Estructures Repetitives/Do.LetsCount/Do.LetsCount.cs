﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/5
* DESCRIPTION: Mostra per pantalla tots els números fins a un enter entrat per l'usuari.
*/

using System;

namespace Do.LetsCount
{
    class Program
    {
        static void Main(string[] args)
        {
            int contador = 1;
            int numerolimite = 0;

            Console.WriteLine("Inserta el limite");
            numerolimite = Convert.ToInt32(Console.ReadLine());

            do
            {
                Console.Write(contador);
                contador++;
            } while (contador <= numerolimite);
        }
    }
}
