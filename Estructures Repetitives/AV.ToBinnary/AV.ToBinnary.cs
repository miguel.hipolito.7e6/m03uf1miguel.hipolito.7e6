﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/19
* DESCRIPTION: Escriu un programa que llegeixi un nombre natural més petit que 256 i escrigui la seva representació en binari.
*/

using System;

namespace AV.ToBinnary
{
    class Program
    {
        static void Main(string[] args)
        {
            int nombre;
            int binari;
            double resultat = 0;
            int j = 0;

            Console.WriteLine("Inserta un enter:");
            nombre = Convert.ToInt32(Console.ReadLine());

            for(; nombre != 1 ; nombre = nombre / 2)
            { //importante cambiar el orden con j
                binari = nombre % 2;
                resultat = binari * Math.Pow(10, j) + resultat; 
                j++;
            }
            Console.Write("1"+resultat);
        }
    }
}
