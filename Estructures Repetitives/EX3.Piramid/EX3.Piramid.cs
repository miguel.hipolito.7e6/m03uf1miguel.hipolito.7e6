﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/13
* DESCRIPTION: S'imprimeix una piràmide d'altura N de #
*/

using System;

namespace EX3.Piramid
{
    class Program
    {
        static void Main(string[] args)
        {
            int limite;

            Console.WriteLine("Inserta N");
            limite = Convert.ToInt32(Console.ReadLine());

            for (int a = 1; a <= limite; a++)
            {
                for (int b = 1; b <= a; b++)  //usar a y no LIMITE.
                {
                    Console.Write("#");
                }
                Console.WriteLine();
            }
        }
    }
}
