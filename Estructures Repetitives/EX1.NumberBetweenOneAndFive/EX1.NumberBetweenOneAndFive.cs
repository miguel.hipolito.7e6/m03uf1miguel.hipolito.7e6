﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/04
* DESCRIPTION: Demanar a l'usuari un enter entre 1 i 5. Si introdueix un número més gran o més petit, torna-li a demanar.
*/

using System;

namespace EX1.NumberBetweenOneAndFive
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;
           
            Console.WriteLine("Inserta un numero entre 1 i 5");
            num = Convert.ToInt32(Console.ReadLine());

            while (num < 1 || num > 5)
            {
                Console.WriteLine("Intentalo de nuevo");
                num = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine($"El numero introduit:{num}");
        }
    }  ////utilizar otra variable stop, es desperdicio
}
