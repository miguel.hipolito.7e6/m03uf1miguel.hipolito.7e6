﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/13
* DESCRIPTION: Fer un programa que llegeixi un nombre enter positiu n i escrigui la suma de les seves xifres
*/

using System;

namespace EX3.SumMyNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int nombre;
            int a;
            int resultado = 0;

            Console.WriteLine("Inserta un numero:");
            nombre = Convert.ToInt32(Console.ReadLine());

            for (; nombre != 0; nombre = nombre / 10)
            {
                a = nombre % 10 + resultado;
                resultado = a;
            }
            // no hace flta usar un contador, utlizando el mismo numero como LIMITE ya vale.
            Console.WriteLine("la suma de sus digitos son:" + resultado);
        }
    }
}
