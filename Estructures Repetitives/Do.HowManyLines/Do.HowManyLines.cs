﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/5
* DESCRIPTION: Volem comptar quantes línies té un text introduït per l'usuari.
*/

using System;

namespace Do.HowManyLines
{
    class Program
    {
        static void Main(string[] args)
        {
            int linias = 0;
            string END = "a";

            Console.WriteLine("Escriu una linia");
            END = Console.ReadLine();

            do
            {
                Console.WriteLine("Escriu una linia");
                END = Console.ReadLine();
                linias++;
            } while (END != "END");
            Console.WriteLine(linias);
        }
    }
}
