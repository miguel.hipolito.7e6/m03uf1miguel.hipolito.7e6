﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/18
* DESCRIPTION: Donat un nombre enter positiu, escriu els nombres de Fibonacci (Lleonard de Pisa) inferiors o iguals a ell.
*/

using System;

namespace AV_
{
    class Program
    {
        static void Main(string[] args)
        {
            int max;
            int intercanvi;

            Console.WriteLine("Inserta enter limit de fibonacci");
            max = Convert.ToInt32(Console.ReadLine());
            int fibonacci1 = 1;
            int fibonacci2 = 1;
            Console.Write(fibonacci2);

            for (; fibonacci2 <= max; fibonacci1 = intercanvi)
            {
                intercanvi = fibonacci2;
                Console.Write("-" + fibonacci2);

                fibonacci2 = fibonacci1 + fibonacci2;
            }
        }
    }
}
