﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/11
* DESCRIPTION: L'usuari introdueix un enter (N) i es mostra per pantalla un compte enrere de N fins a 1.
*/

using System;

namespace EX2.CountDown
{
    class Program
    {
        static void Main(string[] args)
        {
            int limite;

            Console.WriteLine("Inserta enter per un compte enrere");
            limite = Convert.ToInt32(Console.ReadLine());

            for (int a = limite; a > 0 ; a--)
            {
                Console.WriteLine(a);
            }
        }
    }
}
