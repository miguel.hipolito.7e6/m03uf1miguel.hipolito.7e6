﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/18
* DESCRIPTION: Calcula el factorial d'un valor
*/

using System;


namespace AV.Factorial
{
    class Program
    {
        static void Main(string[] args)
        {
            int nombre;
            int resultat;

            Console.WriteLine("Inserta un nombre per fer el seu factorial");
            nombre = Convert.ToInt32(Console.ReadLine());
            resultat = 1; //importante 

            for (; nombre != 0; nombre--)
            {
                resultat = nombre * (resultat);
            }
            Console.WriteLine("resultat es:" + resultat);
        }
    }
}
