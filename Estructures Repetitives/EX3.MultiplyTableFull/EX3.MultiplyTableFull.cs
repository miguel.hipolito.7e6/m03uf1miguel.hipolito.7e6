﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/13
* DESCRIPTION: Imprimeix les taules de multiplicar en forma de taula
*/

using System;
namespace EX3.MultiplyTableFull
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int a = 1; a < 10; a++)
            {
                for (int b= 0; b < 10; b++) //para que salga horizontal 
                {
                    Console.Write(a+b*a);
                    Console.Write("-");
                }
                Console.WriteLine();
            }
        }
    }
}
