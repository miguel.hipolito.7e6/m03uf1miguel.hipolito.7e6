﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/12
* DESCRIPTION: L'usuari introdueix dos valors enters, el final i el salt

Escriu tots els numeros des de l'1 fins al final, amb una distància de salt
*/

using System;

namespace EX3.CountWithJumps
{
    class Program
    {
        static void Main(string[] args)
        {
            int final;
            int salt;

            Console.WriteLine("Inserta nombre destí i el salt partint del 1");
            final = Convert.ToInt32(Console.ReadLine());
            salt = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("--------");

            for (int inicio = 1; inicio <= final; inicio +=salt)
            {
                Console.Write(inicio+",");
            }
        }
    }
}
