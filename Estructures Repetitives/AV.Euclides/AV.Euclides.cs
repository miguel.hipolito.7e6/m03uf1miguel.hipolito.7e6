﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/19
* DESCRIPTION: Implementa l'algorisme d'Euclides, que calcula le màxim comú divisor de dos nombres positius.
*/

using System;

namespace AV.Euclides
{
    class Program
    {
        static void Main(string[] args)
        {
            int min;
            int max;
            
            Console.WriteLine("inserta el grande y el pequeño para mcd:");
            max = Convert.ToInt32(Console.ReadLine());
            min = Convert.ToInt32(Console.ReadLine());
            int resto = max % min;
            int a = 0;

            for (; resto != 0 ; min = a)
            {
                a = resto;
                resto = min % resto;
            }
            Console.WriteLine(a);
        }
    }
}
