﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/19
* DESCRIPTION: Implementa un programa que mostre true si el nombre introduït és primer, false en qualsevol altre cas.
*/

using System;

namespace AV.IsPrime
{
    class Program
    {
        static void Main(string[] args)
        {
            int nombre;
            int parar = 1;
            int a=2;

            Console.WriteLine("Introdueix un primer:");
            nombre = Convert.ToInt32(Console.ReadLine());

            for (; parar != 0 && a < nombre; a++)
            {                      //si es primo, PARAR siempre vale 1 (hasta que a >= nombre)
                parar = nombre % a; 
            }                      //si no es primo, PARAR valdrá 0

            if (parar == 0)
            {
                Console.WriteLine("NO es primer");
            }
            else Console.WriteLine("ES primer");
        }
    }
}
