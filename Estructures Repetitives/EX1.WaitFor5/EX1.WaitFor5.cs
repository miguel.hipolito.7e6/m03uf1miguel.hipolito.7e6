﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/05
* DESCRIPTION: Farem un programa que demana repetidament a l'usuari un enter fins que entri el número 5.
*/

using System;

namespace EX1.WaitFor5
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;

            Console.WriteLine("Inserta el numero");
            num = Convert.ToInt32(Console.ReadLine());

            while (num != 5)
            {
                Console.WriteLine("Inserta el numero");
                num = Convert.ToInt32(Console.ReadLine());
            }
            Console.WriteLine("trobat el 5");
        }
    } //utilizar otra variable stop, es desperdicio
}
