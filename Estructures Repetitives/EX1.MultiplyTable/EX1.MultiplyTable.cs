﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/04
* DESCRIPTION: Volem printar les taules de multiplicar.
*/

using System;

namespace EX1.MultiplyTable
{
    class Program
    {
        static void Main(string[] args)
        {
            int Tabla = 0;
            int limit = 1;

            Console.WriteLine("Inserta el numero");
            Tabla = Convert.ToInt32(Console.ReadLine());

            while (limit < 10)
            {
                Console.WriteLine($"{limit}*{Tabla}= {limit * Tabla}");
                limit++;
            }
        }
    }
}
