﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/05/10
* DESCRIPTION: Farem un programa que demana repetidament a l'usuari un enter fins que entri el número 5.
*/

using System;

namespace Do.WaitFor5
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = 0;

            do
            {
                Console.WriteLine("Inserta el numero");
                num = Convert.ToInt32(Console.ReadLine());
            } while (num != 5);
            
            Console.WriteLine("trobat el 5");
        }
    }
}
