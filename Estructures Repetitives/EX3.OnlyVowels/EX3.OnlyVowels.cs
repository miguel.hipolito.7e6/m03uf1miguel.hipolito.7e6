﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/12
* DESCRIPTION: Donada una llista de lletres, imprimeix únicament les vocals que hi hagi.
*/

using System;

namespace EX3.OnlyVowels
{
    class Program
    {
        static void Main(string[] args)
        {
            int nombre;

            char lletres;

            int numvocales = 0;
            string vocales = "";

            Console.Write("Inserta nombre de lletres:");
            nombre = Convert.ToInt32(Console.ReadLine());
            for (int a = 1; a <= nombre; a++)
            {
                lletres = Convert.ToChar(Console.Read());
                switch (lletres)
                {
                    case 'a':
                        numvocales++;
                        vocales += lletres;
                        break;
                    case 'e':
                        numvocales++;
                        vocales += lletres;
                        break;
                    case 'i':
                        numvocales++;
                        vocales += lletres;
                        break;
                    case 'o':
                        numvocales++;
                        vocales += lletres;
                        break;
                    case 'u':
                        numvocales++;
                        vocales += lletres;
                        break;
                    default:
                        break;
                }
            }
            Console.WriteLine("-------------");
            Console.WriteLine("numero de vocales:" + numvocales);
            Console.WriteLine(vocales);
        }
    }
}