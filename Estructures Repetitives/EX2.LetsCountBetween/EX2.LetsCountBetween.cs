﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/11
* DESCRIPTION: L'usuari introdueix dos valors enters. Printa per pantalla tots els valors que hi ha entre els dos valors introduits ordenats de menor a major
*/

using System;

namespace EX2.LetsCountBetween
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            int b;

            Console.WriteLine("Inserta interval entre A i B, sent A < B");
            a = Convert.ToInt32(Console.ReadLine());
            b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("--------");

            if (a > b)
            {
                int c;
                c = a;
                a = b;
                b = c;
            }

            for (a++; a < b; a++)
            {
                Console.WriteLine(a);
            } //he sumado uno para quitar el valor inicial a y he quitado el igual para el b
        }
    }
}
