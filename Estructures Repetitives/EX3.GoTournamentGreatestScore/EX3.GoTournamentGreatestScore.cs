﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/12
* DESCRIPTION: L'usuari introduirà el nom del participant i la puntuació, cada un en una línia.

Quan ja no hi hagi més participants entrara la paraula clau END.

Imprimeix per pantalla el guanyador del concurs i els punts obtinguts.
*/

using System;

namespace EX3.GoTournamentGreatestScore
{
    class Program
    {
        static void Main(string[] args)
        {
            string END = "a";
            string nombrecomp;
            int puntcomp;
            string nombreganador = "a";
            int puntganador = 0;

            do
            {
                Console.WriteLine("Introduce nombre participante");
                nombrecomp = Console.ReadLine();
                Console.WriteLine("Puntuación:");
                puntcomp = Convert.ToInt32(Console.ReadLine());

                if (puntcomp > puntganador) //dado que puntganador al inicio vale 0, siempre toma esta ruta
                {
                    nombreganador = nombrecomp;
                    puntganador = puntcomp;
                    
                }
                Console.Write("Para continuar enter, o para acabar END: ");
                END = Console.ReadLine();
                Console.WriteLine("-----------");
            } while (END != "END");

            Console.WriteLine("-----------");
            Console.WriteLine("MEJOR PUNTUACIÓN");
            Console.WriteLine(nombreganador +":"+puntganador);
        }
    }
}
