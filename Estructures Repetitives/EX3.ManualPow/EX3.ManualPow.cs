﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/12
* DESCRIPTION:L'usuari introdueix dos enters, la base i l'exponent. Imprimeix el resultat de la potencia (sense usar la llibreria math).
*/

using System;

namespace EX3.ManualPow
{
    class Program
    {
        static void Main(string[] args)
        {
            int Base = 0;
            int exponente = 0;
            int cadena; 

            Console.WriteLine("Inserta la base i el exponent");
            Base = Convert.ToInt32(Console.ReadLine());
            exponente = Convert.ToInt32(Console.ReadLine());

            cadena = Base;

            for (int stop = 1; stop < exponente; stop++)
            {
                Base = Base * cadena;
            }

            Console.WriteLine(Base);
        }
    }
}
