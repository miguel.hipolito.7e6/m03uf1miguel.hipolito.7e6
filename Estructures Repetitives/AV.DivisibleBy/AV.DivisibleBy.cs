﻿/*
* AUTHOR: Miguel Hipólito Fabián
* DATE: 2022/10/18
* DESCRIPTION: Imprimeix tots els nombres enters divisibles per 3 que hi ha entre A i B (inclusiu).
*/

namespace AV.DivisibleBy
{
    class Program
    {
        static void Main(string[] args)
        {
            int min;
            int max;

            Console.WriteLine("Inserta un interval de menor a major:");
            min = Convert.ToInt32(Console.ReadLine());
            max = Convert.ToInt32(Console.ReadLine());
            while (min % 3 != 0)
            {
                min++;
            }

            for (; min <= max; min += 3)
            {
                Console.Write(min + "-");
            }
        }
    }
}
