﻿/*
 * AUTHOR: Miguel Á. Hipólito
 * DATE: 2022/11/18
 * DESCRIPTION: Menu per navegar entre métodes que ultitzen vectors
 */

using System;

namespace Matrius
{
    class Matrius
    {
        //Donada la següent configuració del joc Enfonsar la flota, indica si a la posició (x, y) hi ha aigua o un vaixell (tocat)
        public void SimpleBattleshipResult()
        {
            char[,] tabler = new char[7, 7] { { 'x', 'x', '0', '0', '0', '0', 'x' }, { '0', '0', 'x', '0', '0', '0', 'x' }, { '0', '0', '0', '0', '0', '0', 'x' }, { '0', 'x', 'x', 'x', '0', '0', 'x' }, { '0', '0', '0', '0', 'x', '0', '0' }, { '0', '0', '0', '0', 'x', '0', '0' }, { 'x', '0', '0', '0', '0', '0', '0' }, };

            int i;
            int j;

            do
            {
                Console.Write("Inserta la coordenada X per enfosar (0-6): ");
                i = Convert.ToInt32(Console.ReadLine());

            } while (i < 0 || i > 6);

            do
            {
                Console.Write("Inserta la coordenada Y per enfosar (0-6): ");
                j = Convert.ToInt32(Console.ReadLine());

            } while (i < 0 || i > 6);

            Console.WriteLine("--------");

            if (tabler[i, j] == 'x')
            {
                Console.WriteLine("tocat");
            }
            else Console.WriteLine("aigua");
        }

        //Donada la següent matriu, Imprimeix la suma de tots els seus valors.
        public void MatrixElementSum()
        {
            int[,] valors = new int[3, 4] { { 2, 5, 1, 6 }, { 23, 52, 14, 36 }, { 23, 75, 81, 64 } };

            int resultat = 0;

            for (int i = 0; i < valors.GetLength(0); i++)
            {
                for (int j = 0; j < valors.GetLength(1); j++)
                {
                    resultat = resultat + valors[i, j];
                }
            }
            Console.WriteLine("Donada la matriu {{2,5,1,6},{23,52,14,36},{23,75,81,64}}. La suma és:");
            Console.WriteLine(resultat);
        }

        //Un banc té tot de caixes de seguretat en una graella, numerades per fila i columna del 0 al 3.
        //Volem registrar quan els usuaris obren una caixa de seguretat, i al final del dia, fer-ne un recompte.
        public void MatrixBoxesOpenedCounter()
        {
            int[,] graella = new int[4, 4];

            string stop; //ha de ser string per aceptar enter

            do
            {
                Console.Write("Inserta la coordenada x de la caixa (0-3): ");
                int i = Convert.ToInt32(Console.ReadLine());
                Console.Write("Inserta la coordenada y de la caixa (0-3): ");
                int j = Convert.ToInt32(Console.ReadLine());

                graella[i, j] += 1;

                Console.Write("Si vols parar, escriu [-1]. Sino escriu qualsevol numero: ");
                stop = Console.ReadLine();

            } while (stop != "-1");

            Console.WriteLine("--------");

            for (int i = 0; i < graella.GetLength(0); i++)
            {
                for (int j = 0; j < graella.GetLength(1); j++)
                {
                    Console.Write(graella[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        //Donada la següent matriu, Imprimeix true si algún dels números és divisible entre 13, false altrement.
        public void MatrixThereADiv13()
        {
            int[,] valors = new int[3, 4] { { 2, 5, 1, 6 }, { 23, 52, 14, 36 }, { 23, 75, 81, 62 } };

            bool resultat = false;

            for (int i = 0; i < valors.GetLength(0); i++)
            {
                for (int j = 0; j < valors.GetLength(1); j++)
                {
                    if (valors[i, j] % 13 == 0) resultat = true;
                }
            }
            Console.WriteLine("Donada la matriu {{2,5,1,6},{23,52,14,36},{23,75,81,64}}. Verifiquem si es divisble entre 13:");
            Console.WriteLine(resultat);
        }

        //Digues en quin punt(x,y) es troba el cim més alt i la seva alçada.
        public void HighestMountainOnMap
()
        {
            double[,] valors = new double[5, 5] { { 1.5, 1.6, 1.8, 1.7, 1.6 }, { 1.5, 2.6, 2.8, 2.7, 1.6 }, { 1.5, 4.6, 4.4, 4.9, 1.6 }, { 2.5, 1.6, 3.8, 7.7, 3.6 }, { 1.5, 2.6, 3.8, 2.7, 1.6 } };

            double mesGran = 0;

            int valorX = 0;
            int valorY = 0;

            for (int i = 0; i < valors.GetLength(0); i++)
            {
                for (int j = 0; j < valors.GetLength(1); j++)
                {
                    if (valors[i, j] > mesGran)
                    {
                        mesGran = valors[i, j];
                        valorX = i;
                        valorY = j;
                    }
                }
            }

            Console.WriteLine("El cim més alt és: "+ mesGran);
            Console.WriteLine("Coordenada: (" + valorX + ", " + valorY + ")");
        }

        //El govern britànic ens ha demanat que també vol accedir a les dades de l'exercici anterior i que les necessitaria en peus i no metres.
        public void HighestMountainScalechange()
        {
            double[,] valors = new double[5, 5] { { 1.5, 1.6, 1.8, 1.7, 1.6 }, { 1.5, 2.6, 2.8, 2.7, 1.6 }, { 1.5, 4.6, 4.4, 4.9, 1.6 }, { 2.5, 1.6, 3.8, 7.7, 3.6 }, { 1.5, 2.6, 3.8, 2.7, 1.6 } };

            double mesGran = 0;

            int valorX = 0;
            int valorY = 0;

            for (int i = 0; i < valors.GetLength(0); i++)
            {
                for (int j = 0; j < valors.GetLength(1); j++)
                {
                    if (valors[i, j] > mesGran)
                    {
                        mesGran = valors[i, j];
                        valorX = i;
                        valorY = j;
                    }
                }
            }

            Console.WriteLine("Tenim la matriu: ");

            for (int i = 0; i < valors.GetLength(0); i++)
            {
                for (int j = 0; j < valors.GetLength(1); j++)
                {
                    Console.Write(Math.Round(valors[i, j] * 3.2808, 2).ToString("#.00") + "\t");
                }
                Console.WriteLine();
            }

            Console.WriteLine("--------------");
            Console.WriteLine("El cim més alt és: " + Math.Round(mesGran * 3.2808, 2).ToString("#.00"));
            Console.WriteLine("Coordenada: (" + valorX + ", " + valorY + ")");
        }

        //Implementa un programa que demani dos matrius a l'usuari i imprimeixi la suma de les dues matrius.
        public void MatrixSum()
        {
            int iMatriu1;
            int jMatriu1;

            do
            {
                Console.WriteLine("Introdueix la i, j de la primera matriu:");
                Console.Write("[i]: ");
                iMatriu1 = Convert.ToInt32(Console.ReadLine());
                Console.Write("[j]: ");
                jMatriu1 = Convert.ToInt32(Console.ReadLine());

            } while (iMatriu1 <= 0 || jMatriu1 <= 0);

            int[,] matriu1 = new int[iMatriu1, jMatriu1];
            
            Console.WriteLine("Introdueix el valors de la primera matriu:");
            for (int i = 0; i < iMatriu1; i++)
            {
                for (int j = 0; j < jMatriu1; j++)
                {
                    Console.Write("Valor [" + i + "], [" + j + "]: ");
                    matriu1[i,j] = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine("");
            }

            int iMatriu2;
            int jMatriu2;

            Console.WriteLine("-----------------");
            do
            {
                Console.WriteLine("Introdueix la i, j de la segona matriu:");
                Console.Write("[i]: ");
                iMatriu2 = Convert.ToInt32(Console.ReadLine());
                Console.Write("[j]: ");
                jMatriu2 = Convert.ToInt32(Console.ReadLine());

            } while (iMatriu2 != iMatriu1 || jMatriu2 != jMatriu1);
            
            int[,] matriu2 = new int[iMatriu2, jMatriu2];
           
            Console.WriteLine("Introdueix el valors de la segona matriu:");
            for (int i = 0; i < iMatriu2; i++)
            {
                for (int j = 0; j < jMatriu2; j++)
                {
                    Console.Write("Valor [" + i + "], [" + j + "]: ");
                    matriu2[i,j] = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine("");
            }

            Console.WriteLine("-----------------");
            Console.WriteLine("La matriu suma és: ");


            for (int i = 0; i < iMatriu1; i++)
            {
                for (int j = 0; j < jMatriu1; j++)
                {
                    Console.Write((matriu1[i, j] + matriu2[i, j]) + " ");
                }
                Console.WriteLine();
            }
        }

        //Programa una funció que donat un tauler d'escacs, i una posició, ens mostri per pantalla quines són les possibles posicions a les que es pot moure una torre.
        public void RookMoves()
        {
            string[,] tauler = new string[8, 8] { {"x", "x", "x", "x", "x", "x", "x", "x"}, { "x", "x", "x", "x", "x", "x", "x", "x" }, { "x", "x", "x", "x", "x", "x", "x", "x" }, { "x", "x", "x", "x", "x", "x", "x", "x" }, { "x", "x", "x", "x", "x", "x", "x", "x" }, { "x", "x", "x", "x", "x", "x", "x", "x" }, { "x", "x", "x", "x", "x", "x", "x", "x" }, { "x", "x", "x", "x", "x", "x", "x", "x" }};

            Console.WriteLine("Introdueix la fila i la columna per (T):");

            int intFila;
            int columna;

            do
            {
                Console.Write("[i]: ");
                string fila = Console.ReadLine();

                fila = fila.ToUpper();
                switch (fila)
                {
                    case "A":
                        intFila = 0;
                        break;
                    case "B":
                        intFila = 1;
                        break;
                    case "C":
                        intFila = 2;
                        break;
                    case "D":
                        intFila = 3;
                        break;
                    case "E":
                        intFila = 4;
                        break;
                    case "F":
                        intFila = 5;
                        break;
                    case "G":
                        intFila = 6;
                        break;
                    case "H":
                        intFila = 7;
                        break;
                    default:
                        intFila = 8;
                        break;
                }
            } while (intFila == 8);

            do 
            {
                Console.Write("[j]: ");
                columna = Convert.ToInt32(Console.ReadLine());

            } while (columna < 0 || columna > 7);
            
            Console.WriteLine("----------------------");

            tauler[intFila, columna] = "T";

            for (int j = 0; j < tauler.GetLength(1); j++) //posar la columna
            {
                for (int J = 0; J < tauler.GetLength(1); J++)
                {
                    if (tauler[intFila, J] == "T") tauler[intFila, j] = "c";
                    tauler[intFila, columna] = "T";
                }
            }
            for (int i = 0; i < tauler.GetLength(0); i++) //posar la fila
            {
                for (int I = 0; I < tauler.GetLength(0); I++)
                {
                    if (tauler[I, columna] == "T") tauler[i, columna] = "c";
                    tauler[intFila, columna] = "T";
                }
            }

            for (int i = 0; i < tauler.GetLength(0); i++) //imprimir tauler
            {
                for (int j = 0; j < tauler.GetLength(1); j++)
                {
                    Console.Write(tauler[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        //Donada una matriu quadrada, el programa imprimeix true si la matriu és simètrica, false en cas contrari.
        public void MatrixSimetric()
        {
            int i;
            int j;
            do
            {
                Console.WriteLine("Escriu la i i la j de la matriu QUADRADA:");
                Console.Write("[i]: ");
                i = Convert.ToInt32(Console.ReadLine());
                Console.Write("[j]: ");
                j = Convert.ToInt32(Console.ReadLine());

            } while ( i != j);

            int[,] valors = new int [i, j];
            int[,] trasposada = new int[i, j];
            bool resultat = true;

            Console.WriteLine("Inserta el valors per fila:");          

            for (i = 0; i < valors.GetLength(0); i++) //inserta valors 
            {
                for (j = 0; j < valors.GetLength(1); j++)
                {
                    Console.Write("Valor ["+ i +"], ["+ j +"]: ");
                    valors[i, j] = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine();
            }
            
            for (i = 0; i < valors.GetLength(0); i++) //trasposada i comprovació
            {
                for (j = 0; j < valors.GetLength(1); j++)
                {
                    trasposada[i, j] = valors[j, i];
                    if (valors[i,j] != trasposada [i,j])
                    {
                        resultat = false;
                    }
                }
            }

            Console.WriteLine(resultat);
        }

        //Les matrius es poden multiplicar entre elles mentre siguin de quadrades de igual dimensió o interc
        public void MultiplicacióDeMatrius()
        {
            int iMultiplicant;
            int jMultiplicant; 

            do
            {
                Console.WriteLine("Introdueix la i, j de la primera matriu:");
                Console.Write("[i]: ");
                iMultiplicant = Convert.ToInt32(Console.ReadLine());
                Console.Write("[j]: ");
                jMultiplicant = Convert.ToInt32(Console.ReadLine());

            } while (iMultiplicant <= 0 || jMultiplicant <= 0);

            int[,] multiplicant = new int[iMultiplicant, jMultiplicant];

            Console.WriteLine("Introdueix el valors de la primera matriu:");
            for (int i = 0; i < iMultiplicant; i++)
            {
                for (int j = 0; j < jMultiplicant; j++)
                {
                    Console.Write("Valor [" + i + "], [" + j + "]: ");
                    multiplicant[i, j] = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine("");
            }
            Console.WriteLine("-----------------");

            int iMultiplicador;
            int jMultiplicador;

            do
            {
                Console.WriteLine("Introdueix la i, j de la segona matriu:");
                Console.Write("[i]: ");
                iMultiplicador = Convert.ToInt32(Console.ReadLine());
                Console.Write("[j]: ");
                jMultiplicador = Convert.ToInt32(Console.ReadLine());

            } while ( jMultiplicant != iMultiplicador); //[3,2] [2,3]

            int[,] multiplicador = new int[iMultiplicador, jMultiplicador];

            Console.WriteLine("Introdueix el valors de la segona matriu:");
            for (int i = 0; i < iMultiplicador; i++)
            {
                for (int j = 0; j < jMultiplicador; j++)
                {
                    Console.Write("Valor [" + i + "], [" + j + "]: ");
                    multiplicador[i, j] = Convert.ToInt32(Console.ReadLine());
                }
                Console.WriteLine("");
            }
            Console.WriteLine("-----------------");

            int[,] resultat = new int[iMultiplicant, jMultiplicador]; // matriu resultant

            int multiplicacio = 0;
            
            for (int fila = 0; fila < iMultiplicant; fila++)  //3 vegades
            {
                for (int columna = 0; columna < jMultiplicador; columna++) //3 vegades
                {
                    for (int contador = 0; contador < jMultiplicant; contador++) // 2 vegades
                    {
                        multiplicacio = multiplicacio + multiplicant[fila, contador] * multiplicador[contador, columna];
                    }
                    resultat[fila, columna] = multiplicacio;
                    multiplicacio = 0;
                }
            }   

            for (int i = 0; i < iMultiplicant; i++)
            {
                for (int j = 0; j < jMultiplicador; j++)
                {
                    Console.Write(resultat[i,j] + "  ");
                }
                Console.WriteLine();
            }
        }

        public void Menu()
        {
            string opcio;
            do
            {
                Console.WriteLine("0.- FINALITZAR");
                Console.WriteLine("1.- SimpleBattleshipResult");
                Console.WriteLine("2.- MatrixElementSum");
                Console.WriteLine("3.- MatrixBoxesOpenedCounter");
                Console.WriteLine("4.- MatrixThereADiv13");
                Console.WriteLine("5.- HighestMountainOnMap");
                Console.WriteLine("6.- HighestMountainScalechange");
                Console.WriteLine("7.- MatrixSum");
                Console.WriteLine("8.- RookMoves");
                Console.WriteLine("9.- MatrixSimetric");
                Console.WriteLine("A.- MultiplicacióDeMatrius");

                opcio = Console.ReadLine();
                opcio = opcio.ToUpper();
                Console.Clear();
                switch (opcio)
                {
                    case "0":
                        Console.WriteLine("bye");
                        break;
                    case "1":
                        SimpleBattleshipResult();
                        break;
                    case "2":
                        MatrixElementSum();
                        break;
                    case "3":
                        MatrixBoxesOpenedCounter();
                        break;
                    case "4":
                        MatrixThereADiv13();
                        break;
                    case "5":
                        HighestMountainOnMap();
                        break;
                    case "6":
                        HighestMountainScalechange();
                        break;
                    case "7":
                        MatrixSum();
                        break;
                    case "8":
                        RookMoves();
                        break;
                    case "9":
                        MatrixSimetric();
                        break;
                    case "A":
                        MultiplicacióDeMatrius();
                        break;
                    default:
                        Console.WriteLine("Opció Incorrecta");
                        break;
                }
                Console.ReadLine();
                Console.Clear();

            } while (opcio != "0");
        }

        public static void Main()
        {
            var menu = new Matrius();  
            menu.Menu();              
        }
    }
}
